package com.br.ssq.teste.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.br.ssq.models.Funcionario;
import com.br.ssq.repositories.FuncionarioRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class FuncionarioRepositoryTest {
	
	@Autowired
	private FuncionarioRepository repository;
	
	@Test
	public void findByCPF() {
		
		Funcionario funcionario = repository.findByCpf("11111111111");		
		assertEquals("11111111111", funcionario.getCpf());
		
	}
	
	@Test
	public void findByDepartamento() {
		
		boolean sucesso = false;
		List<Funcionario> funcionarios = repository.findByDepartamentoNome("Desenvolvimento");
		if(funcionarios.size() > 0) {
			sucesso  =true;
		}
		
		assertTrue(sucesso);
	}
	
	@Test
	public void findBySkill() {
		
		boolean sucesso = false;
		List<Funcionario> funcionarios = repository.findByFuncionarioSkillsSkillNome("Java");
		
		if(funcionarios.size() > 0) {
			sucesso  =true;
		}
		
		assertTrue(sucesso);
		
	}
		
}
