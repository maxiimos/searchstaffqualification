package com.br.ssq.teste.resource;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.br.ssq.models.Endereco;
import com.br.ssq.teste.utils.ConvertJsonObject;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EnderecoResourceTest {

	@Autowired
	private MockMvc mvc;
		
	String uri = "/enderecos";
	
	@Test
	public void saveEndereco() throws Exception {
		
		Endereco endereco = new Endereco("Rua Joao Pereira","06608050", "Jandira", "SP", "Brasil", "","2010","analandia");
		mvc.perform(post(uri)
			.contentType(MediaType.APPLICATION_JSON)
			.content(ConvertJsonObject.asJsonString(endereco)))
			.andExpect(status().isCreated());
	}

	@Test
	public void getEndereco() throws Exception{

		mvc.perform(get(uri+"/2")
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.id", is(2)))
			.andExpect(jsonPath("$.endereco", is("Rua Jardim")))
			.andExpect(jsonPath("$.cep", is("06608050")))
			.andExpect(jsonPath("$.cidade", is("Jandira")))
			.andExpect(jsonPath("$.estado", is("SP")))
			.andExpect(jsonPath("$.pais", is("Brasil")))
			.andExpect(jsonPath("$.complemento", is("")))
			.andExpect(jsonPath("$.numero", is("111")))
			.andExpect(jsonPath("$.bairro", is("analandia")))
			.andExpect(status().isOk());
		
	}
	
	@Test
	public void updateEndereco() throws Exception{
		
		Endereco endereco = new Endereco(3,"Rua Joao Pereira","06608050", "Jandira", "SP", "Brasil", "","2010","bairro");
		mvc.perform(put(uri+"/3")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(ConvertJsonObject.asJsonString(endereco)))
				.andExpect(jsonPath("$.id", is(3)))
				.andExpect(jsonPath("$.endereco", is("Rua Joao Pereira")))
				.andExpect(jsonPath("$.cep", is("06608050")))
				.andExpect(jsonPath("$.cidade", is("Jandira")))
				.andExpect(jsonPath("$.estado", is("SP")))
				.andExpect(jsonPath("$.pais", is("Brasil")))
				.andExpect(jsonPath("$.complemento", is("")))
				.andExpect(jsonPath("$.numero", is("2010")))
				.andExpect(jsonPath("$.bairro", is("bairro")))
				.andExpect(status().isOk());
	}
	

	@Test
	public void deleteEndereco() throws Exception{

		mvc.perform(delete(uri+"/4")
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(status().isNoContent());
		
	}
	
	@Test
	public void listEndereco() throws Exception{

		mvc.perform(get(uri)
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$[0].id", is(2)))
			.andExpect(status().isOk());
		
	}
	
}
