package com.br.ssq.teste.resource;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.br.ssq.models.Skill;
import com.br.ssq.teste.utils.ConvertJsonObject;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SkillResourceTest {

	@Autowired
	private MockMvc mvc;
		
	String uri = "/skills";
	
	@Test
	public void saveSkill() throws Exception {
		
		Skill skill = new Skill("Teste", "Teste Save");
		mvc.perform(post(uri)
			.contentType(MediaType.APPLICATION_JSON)
			.content(ConvertJsonObject.asJsonString(skill)))
			.andExpect(status().isCreated());
	}

	@Test
	public void getSkill() throws Exception{

		mvc.perform(get(uri+"/2")
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.id", is(2)))
			.andExpect(jsonPath("$.nome", is("Java")))
			.andExpect(jsonPath("$.descricao", is("Spring")))
			.andExpect(status().isOk());
		
	}
	
	@Test
	public void updateSkill() throws Exception{
		
		Skill skill = new Skill(3,"Teste Update", "Teste Update Correto");		
		mvc.perform(put(uri+"/3")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(ConvertJsonObject.asJsonString(skill)))
				.andExpect(jsonPath("$.id", is(3)))
				.andExpect(jsonPath("$.nome", is("Teste Update")))
				.andExpect(jsonPath("$.descricao", is("Teste Update Correto")))
				.andExpect(status().isOk());
	}
	

	@Test
	public void deleteSkill() throws Exception{

		mvc.perform(delete(uri+"/4")
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(status().isNoContent());
		
	}
	
	@Test
	public void listSkill() throws Exception{

		mvc.perform(get(uri)
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$[0].id", is(2)))
			.andExpect(jsonPath("$[1].id", is(3)))
			.andExpect(status().isOk());
		
	}
	
}
