package com.br.ssq.teste.resource;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.br.ssq.models.FormacaoAcademica;
import com.br.ssq.teste.utils.ConvertJsonObject;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class FormacaoAcademicaResourceTest {

	@Autowired
	private MockMvc mvc;
		
	String uri = "/formacoes";
	
	@Test
	public void saveFormacaoAcademica() throws Exception {
		
		FormacaoAcademica formacaoAcademica = new FormacaoAcademica("Teste", "Teste Save");
		mvc.perform(post(uri)
			.contentType(MediaType.APPLICATION_JSON)
			.content(ConvertJsonObject.asJsonString(formacaoAcademica)))
			.andExpect(status().isCreated());
	}

	@Test
	public void getFormacaoAcademica() throws Exception{

		mvc.perform(get(uri+"/2")
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.id", is(2)))
			.andExpect(jsonPath("$.curso", is("Desenvolvimento 1")))
			.andExpect(jsonPath("$.faculdade", is("UNINOVE")))
			.andExpect(status().isOk());
		
	}
	
	@Test
	public void updateFormacaoAcademica() throws Exception{
		
		FormacaoAcademica formacaoAcademica = new FormacaoAcademica(3,"Teste Update", "Teste Update Correto");		
		mvc.perform(put(uri+"/3")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(ConvertJsonObject.asJsonString(formacaoAcademica)))
				.andExpect(jsonPath("$.id", is(3)))
				.andExpect(jsonPath("$.curso", is("Teste Update")))
				.andExpect(jsonPath("$.faculdade", is("Teste Update Correto")))
				.andExpect(status().isOk());
	}
	

	@Test
	public void deleteFormacaoAcademica() throws Exception{

		mvc.perform(delete(uri+"/4")
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(status().isNoContent())
			.andDo(print());
		
	}
	
	@Test
	public void listFormacaoAcademica() throws Exception{

		mvc.perform(get(uri)
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$[0].id", is(2)))
			.andExpect(jsonPath("$[1].id", is(3)))
			.andExpect(status().isOk());
		
	}
	
}
