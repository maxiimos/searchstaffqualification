package com.br.ssq.teste.resource;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.br.ssq.models.Departamento;
import com.br.ssq.teste.utils.ConvertJsonObject;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DepartamentoResourceTest {
	@Autowired
	private MockMvc mvc;
	
	String uri = "/departamentos";
	
	@Test
	public void saveDepartamento() throws Exception {
		
		Departamento departamento = new Departamento("Teste", "Teste Save");
		mvc.perform(post(uri)
			.contentType(MediaType.APPLICATION_JSON)
			.content(ConvertJsonObject.asJsonString(departamento)))
			.andExpect(status().isCreated());
	}

	@Test
	public void getDepartamento() throws Exception{

		mvc.perform(get(uri+"/2")
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.id", is(2)))
			.andExpect(jsonPath("$.nome", is("Desenvolvimento")))
			.andExpect(jsonPath("$.descricao", is("Desenvolvimento")))
			.andExpect(status().isOk());
		
	}
	
	@Test
	public void updateDepartamento() throws Exception{
		
		Departamento departamento = new Departamento(3,"Teste Update", "Teste Update Correto");		
		mvc.perform(put(uri+"/3")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(ConvertJsonObject.asJsonString(departamento)))
				.andExpect(jsonPath("$.id", is(3)))
				.andExpect(jsonPath("$.nome", is("Teste Update")))
				.andExpect(jsonPath("$.descricao", is("Teste Update Correto")))
				.andExpect(status().isOk());
	}
	

	@Test
	public void deleteDepartamento() throws Exception{

		mvc.perform(delete(uri+"/4")
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(status().isNoContent())
			.andDo(print());
		
	}
	
	@Test
	public void listDepartamento() throws Exception{

		mvc.perform(get(uri)
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$[0].id", is(2)))
			.andExpect(jsonPath("$[1].id", is(3)))
			.andExpect(status().isOk());
		
	}
}
