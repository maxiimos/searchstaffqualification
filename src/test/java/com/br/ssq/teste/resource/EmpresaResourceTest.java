package com.br.ssq.teste.resource;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.br.ssq.models.Empresa;
import com.br.ssq.teste.utils.ConvertJsonObject;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EmpresaResourceTest {

	@Autowired
	private MockMvc mvc;
		
	String uri = "/empresas";
	
	@Test
	public void saveEmpresa() throws Exception {
		
		Empresa empresa = new Empresa("Teste", "Teste Save");
		mvc.perform(post(uri)
			.contentType(MediaType.APPLICATION_JSON)
			.content(ConvertJsonObject.asJsonString(empresa)))
			.andExpect(status().isCreated());
	}

	@Test
	public void getEmpresa() throws Exception{

		mvc.perform(get(uri+"/2")
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.id", is(2)))
			.andExpect(jsonPath("$.nome", is("Empresa 1")))
			.andExpect(jsonPath("$.cnpj", is("11111111111111")))
			.andExpect(status().isOk());
		
	}
	
	@Test
	public void updateEmpresa() throws Exception{
		
		Empresa empresa = new Empresa(3,"Teste Update", "111111111111");		
		mvc.perform(put(uri+"/3")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(ConvertJsonObject.asJsonString(empresa)))
				.andExpect(jsonPath("$.id", is(3)))
				.andExpect(jsonPath("$.nome", is("Teste Update")))
				.andExpect(jsonPath("$.cnpj", is("111111111111")))
				.andExpect(status().isOk());
	}
	

	@Test
	public void deleteEmpresa() throws Exception{

		mvc.perform(delete(uri+"/4")
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(status().isNoContent());
		
	}
	
	@Test
	public void listEmpresa() throws Exception{

		mvc.perform(get(uri)
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$[0].id", is(2)))
			.andExpect(jsonPath("$[1].id", is(3)))
			.andExpect(status().isOk());
		
	}
	
}
