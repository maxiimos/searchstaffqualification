package com.br.ssq.teste.resource;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.br.ssq.models.Funcionario;
import com.br.ssq.teste.utils.ConvertJsonObject;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class FucionarioResourceTest {

	@Autowired
	private MockMvc mvc;
		
	String uri = "/funcionarios";
	
	@Test
	public void saveFuncionario() throws Exception {
		
		SimpleDateFormat sdf =  new SimpleDateFormat("dd/MM/yyyy");
		Date data = sdf.parse("21/04/2018");
		Funcionario funcionario = new Funcionario("Liza","Thompsom","456432212","524444441",data,"liza@gmail.com","Ensino Superior Completo","11911111111","","M", "31270002", "senha");
		mvc.perform(post(uri)
			.contentType(MediaType.APPLICATION_JSON)
			.content(ConvertJsonObject.asJsonString(funcionario)))
			.andExpect(status().isCreated());
	}

	@Test
	public void getFuncionario() throws Exception{

		mvc.perform(get(uri+"/2")
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.id", is(2)))
			.andExpect(jsonPath("$.nome", is("Kelvin")))
			.andExpect(jsonPath("$.sobrenome", is("Santos")))
			.andExpect(jsonPath("$.cpf", is("11111111111")))
			.andExpect(jsonPath("$.rg", is("222222")))
			.andExpect(status().isOk());
		
	}
	
	@Test
	public void updateFuncionario() throws Exception{
		SimpleDateFormat sdf =  new SimpleDateFormat("dd/MM/yyyy");
		Date data = sdf.parse("21/04/2018");
		Funcionario funcionario = new Funcionario(3,"Liza","Thompsom","4564322122","5244444413",data,"liza@gmail.com","Ensino Superior Completo","11911111111","","M","31270002", "senha");		
		mvc.perform(put(uri+"/3")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(ConvertJsonObject.asJsonString(funcionario)))
				.andExpect(jsonPath("$.id", is(3)))
				.andExpect(jsonPath("$.nome", is("Liza")))
				.andExpect(jsonPath("$.sobrenome", is("Thompsom")))
				.andExpect(jsonPath("$.cpf", is("4564322122")))
				.andExpect(jsonPath("$.rg", is("5244444413")))
				.andExpect(status().isOk());
	}
	

	@Test
	public void deleteFuncionario() throws Exception{

		mvc.perform(delete(uri+"/4")
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(status().isNoContent());
		
	}
	
	@Test
	public void listFuncionario() throws Exception{

		mvc.perform(get(uri)
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$[0].id", is(2)))
			.andExpect(status().isOk());
		
	}
	
}
