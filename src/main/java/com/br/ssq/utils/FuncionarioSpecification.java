package com.br.ssq.utils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.br.ssq.models.Departamento;
import com.br.ssq.models.Funcionario;
import com.br.ssq.models.SearchCriteria;

public class FuncionarioSpecification implements Specification<Funcionario>{

	private SearchCriteria criteria;
	
	
	public FuncionarioSpecification(SearchCriteria criteria) {
		super();
		this.criteria = criteria;
	}

	public SearchCriteria getCriteria() {
		return criteria;
	}
	
	@Override
	public Predicate toPredicate(Root<Funcionario> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
		
		String value = ((String) criteria.getValue().toString()).toLowerCase();
		
		if ("skill".equals(criteria.getKey())){
			
			return builder.like(builder.lower(root.join("funcionarioSkills").join("skill").<String>get("nome")),"%" +value+ "%");
		
		}
		else if(root.<String>get(criteria.getKey()).getJavaType() == String.class) {

			return builder.like(builder.lower(root.<String>get(criteria.getKey())),"%"+value+"%");
		
		} else if ("departamento".equals(criteria.getKey())){
		
			return builder.equal(root.<Departamento>get("departamento").get("id"), criteria.getValue() );
		
		}else { 
			
			return builder.equal(root.get(criteria.getKey()), criteria.getValue());
		}
			
		
	}

//	@Override
//	public Predicate toPredicate(Root<Funcionario> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
//		
//		if(criteria.getOperation().equals(">")) {
//			return builder.greaterThanOrEqualTo(root.<String> get(criteria.getKey()), criteria.getValue().toString());
//		} else if (criteria.getOperation().equals("<")) {
//			return builder.lessThanOrEqualTo(root.<String> get(criteria.getKey()), criteria.getValue().toString());
//		} else if (criteria.getOperation().equals(":")) {
//			if(root.<String>get(criteria.getKey()).getJavaType() == String.class) {
//				return builder.like(root.<String>get(criteria.getKey()),"%" + criteria.getValue() + "%");
//			} else {
//				return builder.equal(root.get(criteria.getKey()), criteria.getValue());
//			}
//		}
//		
//		return null;
//	}



}
