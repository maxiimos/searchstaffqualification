package com.br.ssq.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import com.br.ssq.models.Funcionario;
import com.br.ssq.models.SearchCriteria;

public class FuncionarioSpecificationBuilder {

	private final List<SearchCriteria> params;
	
	public FuncionarioSpecificationBuilder() {
		params = new ArrayList<>();
	}
	
	public FuncionarioSpecificationBuilder with(String key, String operation, String value) {
		params.add(new SearchCriteria(key, operation, value));
		return this;
	}
	
	public Specification<Funcionario> build(){
		if(params.size() == 0) {
			return null;
		}
		
		List<Specification<Funcionario>> specs = new ArrayList<Specification<Funcionario>>();
		for(SearchCriteria param : params) {
			System.out.println("Aqui ::::");
			specs.add(new FuncionarioSpecification(param));
		}
		
		Specification<Funcionario> result = specs.get(0);
		for (int i = 1; i < specs.size(); i++) {
			result = Specifications.where(result).and(specs.get(i));
		}
		
		return result;
	}
}
