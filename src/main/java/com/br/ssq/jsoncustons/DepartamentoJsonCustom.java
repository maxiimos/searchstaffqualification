package com.br.ssq.jsoncustons;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;


import com.br.ssq.models.Departamento;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class DepartamentoJsonCustom {
	
	public static class Serializer extends JsonSerializer<Set<Departamento>>{

		@Override
		public void serialize(Set<Departamento> value, JsonGenerator gen, SerializerProvider serializers)
				throws IOException, JsonProcessingException {
			   Set<Integer> ids = new HashSet<>();
		        for (Departamento item : value) {
		            ids.add(item.getId());
		        }
		        gen.writeObject(ids);
		    }
		}
		
	
}
