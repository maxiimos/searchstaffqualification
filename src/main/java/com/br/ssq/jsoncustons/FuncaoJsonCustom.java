package com.br.ssq.jsoncustons;

import java.io.IOException;

import com.br.ssq.models.Funcao;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class FuncaoJsonCustom {

	public static class FuncaoSerializer extends JsonSerializer<Funcao>{

		@Override
		public void serialize(Funcao value, JsonGenerator gen, SerializerProvider serializers)
				throws IOException, JsonProcessingException {
				
			gen.writeStartObject();
			gen.writeNumberField("id", value.getId());
			gen.writeStringField("nome", value.getNome());
			gen.writeStringField("descricao", value.getDescricao());
			gen.writeEndObject();
		}
		
		
		
	}
	
}
