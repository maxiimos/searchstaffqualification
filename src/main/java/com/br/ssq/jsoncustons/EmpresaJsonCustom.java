package com.br.ssq.jsoncustons;

import java.io.IOException;

import com.br.ssq.models.Empresa;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class EmpresaJsonCustom {

	public static class EmpresaSerializer extends JsonSerializer<Empresa>{

		@Override
		public void serialize(Empresa value, JsonGenerator gen, SerializerProvider serializers)
				throws IOException, JsonProcessingException {
				
			gen.writeStartObject();
			gen.writeNumberField("id", value.getId());
			gen.writeEndObject();
		}
		
		
		
	}
	
}
