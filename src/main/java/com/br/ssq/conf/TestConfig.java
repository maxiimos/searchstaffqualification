package com.br.ssq.conf;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.br.ssq.services.DBService;

@Configuration
@Profile("test")
public class TestConfig {

	@Autowired
	private DBService DBservice;
	
	@Bean
	public boolean instantiateDataBase() throws ParseException {
		DBservice.instanciateTesteDataBase();
		return true;
	}
//	
//	@Bean
//	public EmailService emailService() {
//		return new MockEmailService();
//	}
	
	
//	@Bean
//	public EmailService emailService() {
//		return new SmtpEmailService();
//	}
	
}
