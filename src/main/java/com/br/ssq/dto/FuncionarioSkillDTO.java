package com.br.ssq.dto;

import com.br.ssq.models.FuncionarioSkill;
import com.br.ssq.models.Skill;

public class FuncionarioSkillDTO {

	private Skill skill;
	private String nivel;
	
	public FuncionarioSkillDTO() {
		
	}

	public FuncionarioSkillDTO(FuncionarioSkill funcionarioSkill) {
		this.skill = funcionarioSkill.getSkill();
		this.nivel = funcionarioSkill.getNivel();
	}
	
	public Skill getSkill() {
		return skill;
	}

	public void setSkill(Skill skill) {
		this.skill = skill;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

}
