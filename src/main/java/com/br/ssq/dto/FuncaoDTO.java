package com.br.ssq.dto;

import com.br.ssq.models.Funcao;

public class FuncaoDTO {

	private int id;
	private String nome; 
	private String descricao;
	
	public FuncaoDTO() {
		super();
	}
	
	public FuncaoDTO(Funcao obj) {
		this.id = obj.getId();
		this.nome = obj.getNome();
		this.descricao = obj.getDescricao();
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
