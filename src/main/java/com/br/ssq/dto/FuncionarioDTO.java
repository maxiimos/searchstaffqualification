package com.br.ssq.dto;

import com.br.ssq.models.Funcionario;

public class FuncionarioDTO {

	private int id;
	private String nome;
	private String sobrenome;
	private String cpf;
	private String rg;
	private String email;
	
	public FuncionarioDTO() {
		
	}
	
	public FuncionarioDTO(Funcionario funcionario) {
		super();
		this.id = funcionario.getId();
		this.nome = funcionario.getNome();
		this.sobrenome = funcionario.getSobrenome();
		this.cpf = funcionario.getCpf();
		this.rg = funcionario.getRg();
		this.email = funcionario.getEmail();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
