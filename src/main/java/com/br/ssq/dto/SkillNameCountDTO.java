package com.br.ssq.dto;

import com.br.ssq.models.relatorios.SkillNameAndCount;

public class SkillNameCountDTO {
	
	private String nome;
	private int qtd;
	
	public SkillNameCountDTO(SkillNameAndCount obj) {
		this.nome = obj.getNome();
		this.qtd = obj.getSkillId();
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public long getQtd() {
		return qtd;
	}
	public void setQtd(int qtd) {
		this.qtd = qtd;
	}	

}
