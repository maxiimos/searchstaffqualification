package com.br.ssq.dto;

import com.br.ssq.models.Departamento;

public class DepartamentoDTO {

	private int id;
	private String nome;
	private String descricao;
	
	public DepartamentoDTO() {
		
	}
	
	public DepartamentoDTO(Departamento departamento) {
		super();
		this.id = departamento.getId();
		this.nome = departamento.getNome();
		this.descricao = departamento.getDescricao();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
