package com.br.ssq.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="ENDERECOS")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Endereco {

	private Integer id;
	private String endereco;
	private String cep;
	private String cidade;
	private String estado;
	private String pais;
	private String complemento;
	private String numero;
	private String bairro;
	
	public Endereco() {
		super();
	}
	
	public Endereco(String endereco, String cep, String cidade, String estado, String pais, String complemento,
			String numero, String bairro) {
		super();
		this.endereco = endereco;
		this.cep = cep;
		this.cidade = cidade;
		this.estado = estado;
		this.pais = pais;
		this.complemento = complemento;
		this.numero = numero;
		this.bairro = bairro;
	}
	
	public Endereco(Integer id, String endereco, String cep, String cidade, String estado, String pais, String complemento,
			String numero,String bairro) {
		super();
		this.id = id;
		this.endereco = endereco;
		this.cep = cep;
		this.cidade = cidade;
		this.estado = estado;
		this.pais = pais;
		this.complemento = complemento;
		this.numero = numero;
		this.bairro = bairro;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	@Override
	public String toString() {
		return "Endereco [id=" + id + ", endereco=" + endereco + ", cep=" + cep + ", cidade=" + cidade + ", estado="
				+ estado + ", pais=" + pais + ", complemento=" + complemento + ", numero=" + numero + ", bairro="
				+ bairro + "]";
	}
	
}
