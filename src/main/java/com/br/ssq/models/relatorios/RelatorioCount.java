package com.br.ssq.models.relatorios;

import java.util.List;

import com.br.ssq.dto.SkillNameCountDTO;

public class RelatorioCount {
	
	private long funcionarioQTD;
	private long funcionarioNewQTD;
	private long skillQTD;
	private long skillNewQTD;
	private List<SkillNameCountDTO> skillNameCount;
	
	public RelatorioCount() {
		super();
	}
	
	public RelatorioCount(long funcionarioQTD, long funcionarioNewQTD, long skillQTD, long skillNewQTD) {
		super();
		this.funcionarioQTD = funcionarioQTD;
		this.funcionarioNewQTD = funcionarioNewQTD;
		this.skillQTD = skillQTD;
		this.skillNewQTD = skillNewQTD;
	}
	
	public RelatorioCount(long funcionarioQTD, long funcionarioNewQTD, long skillQTD, long skillNewQTD,
			List<SkillNameCountDTO> skillNameCount) {
		super();
		this.funcionarioQTD = funcionarioQTD;
		this.funcionarioNewQTD = funcionarioNewQTD;
		this.skillQTD = skillQTD;
		this.skillNewQTD = skillNewQTD;
		this.skillNameCount = skillNameCount;
	}

	public long getFuncionarioQTD() {
		return funcionarioQTD;
	}
	public void setFuncionarioQTD(long funcionarioQTD) {
		this.funcionarioQTD = funcionarioQTD;
	}
	public long getFuncionarioNewQTD() {
		return funcionarioNewQTD;
	}
	public void setFuncionarioNewQTD(long funcionarioNewQTD) {
		this.funcionarioNewQTD = funcionarioNewQTD;
	}
	public long getSkillQTD() {
		return skillQTD;
	}
	public void setSkillQTD(long skillQTD) {
		this.skillQTD = skillQTD;
	}
	public long getSkillNewQTD() {
		return skillNewQTD;
	}
	public void setSkillNewQTD(long skillNewQTD) {
		this.skillNewQTD = skillNewQTD;
	}

	public List<SkillNameCountDTO> getSkillNameCount() {
		return skillNameCount;
	}

	public void setSkillNameCount(List<SkillNameCountDTO> skillNameCount) {
		this.skillNameCount = skillNameCount;
	}
	
}
