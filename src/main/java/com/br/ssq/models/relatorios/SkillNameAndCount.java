package com.br.ssq.models.relatorios;

public interface SkillNameAndCount {

	int getSkillId();
	String getNome();
	
}
