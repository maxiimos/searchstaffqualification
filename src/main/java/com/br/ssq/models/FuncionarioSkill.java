package com.br.ssq.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="FUNCIONARIO_SKILL")
public class FuncionarioSkill implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Funcionario funcionario;
	private Skill skill;
	private String nivel;

	public FuncionarioSkill() {
		super();
	}

	public FuncionarioSkill(Skill skill, String nivel) {
		super();
		this.skill = skill;
		this.nivel = nivel;
	}
	
	public FuncionarioSkill(Funcionario funcionario, Skill skill, String nivel) {
		super();
		this.funcionario = funcionario;
		this.skill = skill;
		this.nivel = nivel;
	}

	
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "funcionario_id")
	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	
	@ManyToOne
	@JoinColumn(name = "skill_id")
	public Skill getSkill() {
		return skill;
	}
	
	public void setSkill(Skill skill) {
		this.skill = skill;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}	
	
}
