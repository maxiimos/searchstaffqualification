package com.br.ssq.models;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.br.ssq.domain.enums.Perfil;
import com.br.ssq.jsoncustons.FuncaoJsonCustom;
import com.br.ssq.services.validation.FuncionarioValidation;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@Entity
@Table(name="FUNCIONARIOS")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@FuncionarioValidation
public class Funcionario implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String nome;
	private String sobrenome;
	private String cpf;
	private String rg;
	private Date dataNascimento;
	private String email;
	private String escolaridade;
	private String celularCorporativo;
	private String descricao;
	private Set<FuncionarioSkill> funcionarioSkills;
	private Funcao funcao;
	private FormacaoAcademica formacaoAcademica;
	private Endereco endereco;
	private Departamento departamento;
	private Date dataCriado;
	private String sexo;
	private String ramal;
	private String senha;
	private Set<Perfil> perfis =  new HashSet<>();
	
	public Funcionario() {
		this.funcionarioSkills = new HashSet<>();		
		addPerfil(Perfil.FUNCIONARIO);
	}
	
	public Funcionario(String nome, String sobrenome, String cpf, String rg, Date dataNascimento, String email,
			String escolaridade, String celularCorporativo, String descricao, String sexo, String ramal, String senha) {
		super();
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.cpf = cpf;
		this.rg = rg;
		this.dataNascimento = dataNascimento;
		this.email = email;
		this.escolaridade = escolaridade;
		this.celularCorporativo = celularCorporativo;
		this.descricao = descricao;
		this.sexo = sexo;
		this.ramal = ramal;
		this.senha = senha;
		addPerfil(Perfil.FUNCIONARIO);
	}	
	
	public Funcionario(Integer id, String nome, String sobrenome, String cpf, String rg, Date dataNascimento, String email,
			String escolaridade, String celularCorporativo, String descricao, String sexo, String ramal, String senha) {
		super();
		this.id = id;
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.cpf = cpf;
		this.rg = rg;
		this.dataNascimento = dataNascimento;
		this.email = email;
		this.escolaridade = escolaridade;
		this.celularCorporativo = celularCorporativo;
		this.descricao = descricao;
		this.sexo = sexo;
		this.ramal = ramal;
		this.senha = senha;
		addPerfil(Perfil.FUNCIONARIO);
	}	
	
	public Funcionario( String nome, String sobrenome, String cpf, String rg, Date dataNascimento, String email,
			String escolaridade, String celularCorporativo, String descricao, String sexo, String ramal, String senha, Date dataCriado) {
		super();
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.cpf = cpf;
		this.rg = rg;
		this.dataNascimento = dataNascimento;
		this.email = email;
		this.escolaridade = escolaridade;
		this.celularCorporativo = celularCorporativo;
		this.descricao = descricao;
		this.sexo = sexo;
		this.ramal = ramal;
		this.senha = senha;
		this.dataCriado = dataCriado;
		addPerfil(Perfil.FUNCIONARIO);
	}	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@NotEmpty(message="Preenchimento Obrigatório")
	@Length(min=3, max=20, message="O tamanho deve ser entre 2 ou 20 caracteres.")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@NotEmpty(message="Preenchimento Obrigatório")
	@Length(min=2, max=30, message="O tamanho deve ser entre 2 ou 30 caracteres.")
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	
	@NotEmpty(message="Preenchimento Obrigatório")
	@Length(min=11, max=11, message="O tamanho deve ser de 11 caracteres.")
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	@NotEmpty(message="Preenchimento Obrigatório")
	@Length(min=2, max=30, message="O tamanho deve ser entre 2 ou 30 caracteres.")
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	@Email(message="E-mail inválido")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEscolaridade() {
		return escolaridade;
	}
	public void setEscolaridade(String escolaridade) {
		this.escolaridade = escolaridade;
	}
	public String getCelularCorporativo() {
		return celularCorporativo;
	}
	public void setCelularCorporativo(String celularCorporativo) {
		this.celularCorporativo = celularCorporativo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@JsonManagedReference
	@OneToMany(mappedBy="funcionario",cascade= {CascadeType.ALL}, orphanRemoval=true)
	public Set<FuncionarioSkill> getFuncionarioSkills() {
		return funcionarioSkills;
	}
	public void setFuncionarioSkills(Set<FuncionarioSkill> funcionarioSkills) {
		this.funcionarioSkills = funcionarioSkills;
	}
	
	
	@JsonSerialize(using=FuncaoJsonCustom.FuncaoSerializer.class)
	@ManyToOne
	public Funcao getFuncao() {
		return funcao;
	}

	public void setFuncao(Funcao funcao) {
		this.funcao = funcao;
	}

	@OneToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY,orphanRemoval=true)
	public FormacaoAcademica getFormacaoAcademica() {
		return formacaoAcademica;
	}

	public void setFormacaoAcademica(FormacaoAcademica formacaoAcademica) {
		this.formacaoAcademica = formacaoAcademica;
	}

	@OneToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY,orphanRemoval=true)
	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@ManyToOne
	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
 
	@JsonIgnore
	public Date getDataCriado() {
		return dataCriado;
	}

	public void setDataCriado(Date dataCriado) {
		this.dataCriado = dataCriado;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getRamal() {
		return ramal;
	}

	public void setRamal(String ramal) {
		this.ramal = ramal;
	}

	@JsonIgnore
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="PERFIS")
	public Set<Perfil> getPerfis(){
		
		return perfis;
	}

	public void setPerfis(Set<Perfil> perfis) {
		this.perfis = perfis;
	}

	public void addPerfil(Perfil perfil) {
		
		this.perfis.add(perfil);
	}

	@Override
	public String toString() {
		return "Funcionario [id=" + id + ", nome=" + nome + ", sobrenome=" + sobrenome + ", cpf=" + cpf + ", rg=" + rg
				+ ", dataNascimento=" + dataNascimento + ", email=" + email + ", escolaridade=" + escolaridade
				+ ", celularCorporativo=" + celularCorporativo + ", descrição=" + descricao + ", funcionarioSkills="
				+ funcionarioSkills + ", funcao=" + funcao + ", formacaoAcademica=" + formacaoAcademica + ", endereco="
				+ endereco + ", departamento=" + departamento + ", dataCriado=" + dataCriado + ", sexo=" + sexo
				+ ", ramal=" + ramal + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	

}

