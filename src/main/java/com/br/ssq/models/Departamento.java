	package com.br.ssq.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.br.ssq.jsoncustons.EmpresaJsonCustom;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name="DEPARTAMENTOS")
public class Departamento {
	
	private Integer id;
	private String nome;
	private String descricao;
	private Set<Funcionario> funcionarios;	
	@JsonSerialize(using=EmpresaJsonCustom.EmpresaSerializer.class)
	private Empresa empresa;	
	
	public Departamento() {
		funcionarios = new HashSet<>();
	}
	
	
	public Departamento(String nome, String descricao) {
		funcionarios = new HashSet<>();
		this.nome = nome;
		this.descricao = descricao;
	}

	

	public Departamento(Integer id, String nome, String descricao) {
		funcionarios = new HashSet<>();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@NotEmpty(message="Preenchimento Obrigatório")
	@Length(min=2, max=30, message="O tamanho deve ser entre 2 ou 30 caracteres.")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Length(max=200, message="O tamanho deve ser de até 200 caracteres")
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	

	@JsonIgnore
	@OneToMany(mappedBy = "departamento")
	public Set<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(Set<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}


	@ManyToOne
	@JoinColumn(name="empresa_id")
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	

	@Override
	public String toString() {
		return "Departamento [id=" + id + ", nome=" + nome + ", descricao=" + descricao + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Departamento other = (Departamento) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
