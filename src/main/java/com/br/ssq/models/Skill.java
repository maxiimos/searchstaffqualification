package com.br.ssq.models;

import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="SKILLS")
public class Skill {
 
	private int id;
	private String nome;
	private String descricao;
	private Set<FuncionarioSkill> funcionarioSkills;
	@JsonIgnore
	private Date criado;

	public Skill() {
		super();
	}
	

	public Skill(String nome, String descricao) {
		super();
		this.nome = nome;
		this.descricao = descricao;
	}
	
	public Skill(int id, String nome, String descricao) {
		super();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
	}

	public Skill(SkillAprovacao skill) {
		this.nome = skill.getNome();
		this.descricao = skill.getDescricao();
		this.criado = skill.getCriado();
	}


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@NotEmpty(message="Preenchimento Obrigatório")
	@Length(min=2, max=30, message="O tamanho deve ser entre 2 ou 30 caracteres.")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Length(max=200, message="O tamanho não pode exceder 200 caracteres.")
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@JsonIgnore
	@OneToMany(mappedBy="skill")
	public Set<FuncionarioSkill> getFuncionarioSkills() {
		return funcionarioSkills;
	}
	
	public void setFuncionarioSkills(Set<FuncionarioSkill> funcionarioSkills) {
		this.funcionarioSkills = funcionarioSkills;
	}
	
	public Date getCriado() {
		return criado;
	}

	public void setCriado(Date criado) {
		this.criado = criado;
	}

	@Override
	public String toString() {
		return "Skill [id=" + id + ", nome=" + nome + ", descricao=" + descricao + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Skill other = (Skill) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	
}
