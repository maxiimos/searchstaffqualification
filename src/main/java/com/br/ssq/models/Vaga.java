package com.br.ssq.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="vagas")
public class Vaga{

	
	private int id;
	private String titulo;
	private String descricao;
	private String diferenciais;
	private String skills;
	private boolean status;
	private Empresa empresa;
	private Date dataCriado;
	
	public Vaga() {
		super();
	}
	
	public Vaga(String titulo, String descricao, String diferenciais, String skills, boolean status) {
		super();
		this.titulo = titulo;
		this.descricao = descricao;
		this.diferenciais = diferenciais;
		this.skills = skills;
		this.status = status;
	}
	
	public Vaga(int id, String titulo, String descricao, String diferenciais, String skills, boolean status) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.descricao = descricao;
		this.diferenciais = diferenciais;
		this.skills = skills;
		this.status = status;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@NotEmpty(message="Preenchimento Obrigatório")
	@Length(min=2, max=40, message="Minimo 2 e máximo de 40 caracteres")
	public String getTitulo() {
		return titulo;
	}
	

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	@NotEmpty(message="Preenchimento Obrigatório")
	@Length(min=10, max=200, message="Minimo 10 e máximo de 200 caracteres")
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDiferenciais() {
		return diferenciais;
	}
	public void setDiferenciais(String diferenciais) {
		this.diferenciais = diferenciais;
	}
	
	@NotEmpty(message="Preenchimento Obrigatório")
	@Length(min=10, max=200, message="Minimo 10 e máximo de 200 caracteres")
	public String getSkills() {
		return skills;
	}
	public void setSkills(String skills) {
		this.skills = skills;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	@ManyToOne
	@JoinColumn(name="empresa_id")
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Date getDataCriado() {
		return dataCriado;
	}

	public void setDataCriado(Date dataCriado) {
		this.dataCriado = dataCriado;
	}
	
}
