package com.br.ssq.models;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name="FORMACOES_ACADEMICAS")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class FormacaoAcademica {

	private Integer id;
	private String curso;
	private String faculdade;
	
	public FormacaoAcademica() {
		super();

	}
	
	public FormacaoAcademica(String curso,String faculdade) {
		super();
		this.faculdade = faculdade;
		this.curso = curso;
	}
	
	public FormacaoAcademica(Integer id, String curso, String faculdade) {
		super();
		this.id = id;
		this.faculdade = faculdade;
		this.curso = curso;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	
	public String getFaculdade() {
		return faculdade;
	}
	public void setFaculdade(String faculdade) {
		this.faculdade = faculdade;
	}

	@Override
	public String toString() {
		return "FormacaoAcademica [id=" + id + ", faculdade=" + faculdade + ", curso=" + curso + "]";
	}
	
	
	
}
