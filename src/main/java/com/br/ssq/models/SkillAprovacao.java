package com.br.ssq.models;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="SKILLS_APROVACAO")
public class SkillAprovacao{

	private int id;
	private String nome;
	private String descricao;
	private boolean aprovado;
	@JsonIgnore
	private Date criado;
	
	public SkillAprovacao() {
		
	}

	public SkillAprovacao(int id, String nome, String descricao, boolean aprovado) {
		super();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
		this.aprovado = aprovado;
	}


	public SkillAprovacao(Skill skill) {
		this.nome = skill.getNome();
		this.descricao = skill.getDescricao();
		this.aprovado = false;
		this.criado = skill.getCriado();
	}
	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@NotEmpty(message="Preenchimento Obrigatório")
	@Length(min=2, max=30, message="O tamanho deve ser entre 2 ou 30 caracteres.")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Length(max=200, message="O tamanho não pode exceder 200 caracteres.")
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public boolean isAprovado() {
		return aprovado;
	}


	public void setAprovado(boolean aprovado) {
		this.aprovado = aprovado;
	}


	public Date getCriado() {
		return criado;
	}

	public void setCriado(Date criado) {
		this.criado = criado;
	}

	
	
	@Override
	public String toString() {
		return "Skill [id=" + id + ", nome=" + nome + ", descricao=" + descricao + "]";
	}
		
	
}
