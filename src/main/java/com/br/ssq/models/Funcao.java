package com.br.ssq.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name="FUNCOES")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Funcao {

	private Integer id;
	private String nome;
	private String descricao;
	private Set<Funcionario> funcionarios;
	private Departamento departamento;

	public Funcao() {
		funcionarios = new HashSet<>();
	}

	public Funcao(String nome, String descricao) {
		funcionarios = new HashSet<>();
		this.nome = nome;
		this.descricao = descricao;
	}
	
	public Funcao(Integer id, String nome, String descricao) {
		funcionarios = new HashSet<>();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@NotEmpty(message="Preenchimento Obrigatório")
	@Length(min=2, max=40, message="Minimo 2 e máximo de 40 caracteres")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	@Length(max=200, message="Máximo de 200 caracteres")
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@JsonIgnore
	@OneToMany(mappedBy = "funcao", cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	public Set<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(Set<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}

	@ManyToOne
	@JoinColumn(name="departamento_id")
	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	@Override
	public String toString() {
		return "Funcao [id=" + id + ", nome=" + nome + ", Descricao=" + descricao + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcao other = (Funcao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
}
