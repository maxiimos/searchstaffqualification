package com.br.ssq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SearchStaffQualificationApplication  {
	
	public static void main(String[] args) {
		SpringApplication.run(SearchStaffQualificationApplication.class, args);
	}
}
