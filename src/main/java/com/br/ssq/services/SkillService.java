package com.br.ssq.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.br.ssq.models.Skill;
import com.br.ssq.models.SkillAprovacao;
import com.br.ssq.repositories.SkillAprovacaoRepository;
import com.br.ssq.repositories.SkillRepository;
import com.br.ssq.services.exceptions.DataIntegrityException;
import com.br.ssq.services.exceptions.ObjectNotFoundException;

@Service
public class SkillService {

	@Autowired
	private SkillRepository repository;
	
	@Autowired
	private SkillAprovacaoRepository aprovacaoRepository;
	
	public Skill find(Integer id) {		
		
		Skill skill = repository.findOne(id);
		
		if(skill == null) {			
			throw new ObjectNotFoundException("Objeto com o ID "+ id +" não foi encontrado");			
		}
		
		return  repository.findOne(id);
	}
	
	public Skill insert(Skill skill) {	
		
		//skill.setCriado(new Date());		
		return  repository.save(skill);
		
	}
	
	public Skill update(Skill skill) {		
		find(skill.getId());
		return repository.save(skill);
	}
	
	public void delete(Integer id) {
		
		find(id);
		
		try {
			repository.delete(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel excluir skill com o ID "+ id +", pois possui funcionarios atrelados ao mesmo");
		}		
	}
	
	public List<Skill> findAll(){
		
		 List<Skill> list = repository.findAll();
		
		if(list.isEmpty()) {
			throw new ObjectNotFoundException("Não existe skills cadastradas no sistema");
		}
		
		return list;
		
	}
	
	public SkillAprovacao insereSkillAprovacao(Skill  skill) {
		skill.setCriado(new Date());
		return aprovacaoRepository.save(new SkillAprovacao(skill));
		
	}
	
	public List<Skill> findSkillByFuncionarioId(Integer id){
		
		List<Skill> list = repository.findByFuncionarioSkillsFuncionarioId(id);
		
		if(list.isEmpty()) {		
			throw new ObjectNotFoundException("Não há skills atreladas ao funcionario com o id "+ id);			
		}
		
		return list ;
	}

	public List<SkillAprovacao> findAllAprovacao() {
		List<SkillAprovacao> list = aprovacaoRepository.findAll();
		
		if(list.isEmpty()) {		
			throw new ObjectNotFoundException("Não há skills para aprovacao");			
		}
		
		return list ;
	}

	public List<SkillAprovacao> insereSkill(SkillAprovacao skill) {
		
		if(skill.isAprovado()) {
			insert(new Skill(skill));
			aprovacaoRepository.delete(skill);
		} 
		
		List<SkillAprovacao> list = aprovacaoRepository.findAll();
		
		if(list.isEmpty()) {		
			throw new ObjectNotFoundException("Não há skills para aprovacao");			
		}
		
		return list ;

	}
	
}
