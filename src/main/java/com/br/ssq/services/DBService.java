package com.br.ssq.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.br.ssq.domain.enums.Perfil;
import com.br.ssq.models.Departamento;
import com.br.ssq.models.Empresa;
import com.br.ssq.models.Endereco;
import com.br.ssq.models.FormacaoAcademica;
import com.br.ssq.models.Funcao;
import com.br.ssq.models.Funcionario;
import com.br.ssq.models.FuncionarioSkill;
import com.br.ssq.models.Skill;
import com.br.ssq.repositories.DepartamentoRepository;
import com.br.ssq.repositories.EmpresaRepository;
import com.br.ssq.repositories.FuncaoRepository;
import com.br.ssq.repositories.FuncionarioRepository;
import com.br.ssq.repositories.SkillRepository;

@Service
public class DBService {

	@Autowired
	private EmpresaRepository empresaRepository;
	
	@Autowired
	private DepartamentoRepository departamentoRepository;
	
	@Autowired
	private FuncaoRepository funcaoRepository;
	
	@Autowired
	private FuncionarioRepository funcionarioRepository;
	
	@Autowired
	private SkillRepository skillRepository;
	
	@Autowired
	private BCryptPasswordEncoder pe;
	
	public void instanciateTesteDataBase () throws ParseException {
		
		Empresa empresa1 =  new Empresa(null,"SSQ", "15209775000149");
		Empresa empresa2 =  new Empresa(null, "ProjectSSQ", "14290638000119");
		
		Departamento departamento1 = new Departamento(null,"Conta", "Departamento de contas");
		Departamento departamento2 = new Departamento(null,"Financeiro", "Departamento Financeiro");
		Departamento departamento3 = new Departamento(null,"T.I", "T.I");
		
		Funcao funcao1 =  new Funcao(null, "Desenvolvedor Java", "Profissional que atua desenvolvendo programas na linguagem Java");
		Funcao funcao2 =  new Funcao(null, "Gerente de Projetos", "Profissional que atua gerenciando os projetos, fazendo toda a comunicação com os Stakeholders");
		Funcao funcao3 =  new Funcao(null, "Gerente de Contas", "Profissional que atua gerenciando as contas da empresa");
		
		departamento1.setEmpresa(empresa1);
		departamento2.setEmpresa(empresa1);
		departamento3.setEmpresa(empresa1);
		
		empresaRepository.save(Arrays.asList(empresa1, empresa2));
		
		departamento1 = departamentoRepository.save(departamento1);
		departamento2 = departamentoRepository.save(departamento2);
		departamento3 = departamentoRepository.save(departamento3);
		
		funcao1.setDepartamento(departamento3);
		funcao2.setDepartamento(departamento3);
		funcao3.setDepartamento(departamento1);
		
		funcaoRepository.save(Arrays.asList(funcao1,funcao2,funcao3));
		
		Skill skill1 = new Skill("Programador Java", "Programador Java");
		Skill skill2 = new Skill("Excel", "Excel");
		Skill skill3 = new Skill("Programador PL/SQL", "Programador PL/SQL");
		Skill skill4 = new Skill("Word", "Word");
		Skill skill5 = new Skill("Avaya", "Avaya");
		
		skillRepository.save(Arrays.asList(skill1,skill2,skill3,skill4,skill5));
		
		SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
		
		Endereco endereco1 = new Endereco(null, "Rua Salomão", "06607041", "Carapicuiba", "SP", "Brasil", "", "21", "Caracas");
		Endereco endereco2 = new Endereco(null, "Av Thomas Turbando Pinto", "02207024", "Grajaú", "SP", "Brasil", "", "2010", "Grajaúex");
		Endereco endereco3 = new Endereco(null, "Rua Joaquim", "01208017", "Tiradentes", "SP", "Brasil", "", "112", "Tiradentex");
		
		FormacaoAcademica formacaoAcademica1 = new FormacaoAcademica("Sistemas de Informação", "Universidade Nove de Julho");
		FormacaoAcademica formacaoAcademica2 = new FormacaoAcademica("Redes", "Universidade Nove de Julho");
		FormacaoAcademica formacaoAcademica3 = new FormacaoAcademica("Administração", "PUC");
		
		Funcionario funcionario1 =  new Funcionario("Kelvin", "Santos", "55606621274", "403289440",  new Date(formater.parse("26/12/1996").getTime()) , "kelvin@gmail.com", "Superior Completo", "11947153077", "Funcionario extremamente competente", "M", "2134", pe.encode("1234"), new Date(formater.parse("23/05/2019").getTime()));
		funcionario1.setFuncao(funcao1);
		funcionario1.setEndereco(endereco1);
		funcionario1.setFormacaoAcademica(formacaoAcademica1);
		funcionario1.setDepartamento(departamento3);
		funcionario1.addPerfil(Perfil.ADMIN);
		
		FuncionarioSkill funcionarioSkill1 =  new FuncionarioSkill(funcionario1,skill4, "Avançado");
		FuncionarioSkill funcionarioSkill2 =  new FuncionarioSkill(funcionario1,skill3, "Básico");
		FuncionarioSkill funcionarioSkill3 =  new FuncionarioSkill(funcionario1,skill5, "Avançado");	
		
		funcionario1.setFuncionarioSkills(Stream.of(funcionarioSkill1, funcionarioSkill2, funcionarioSkill3).collect(Collectors.toSet()));
		
		Funcionario funcionario2 =  new Funcionario("Daniel", "Oliveira", "12953201262", "418757896",  new Date(formater.parse("12/05/1994").getTime()) , "daniel@gmail.com", "Superior Completo", "11956743456", "Funcionario extremamente competente", "M", "2135", pe.encode("1234"), new Date(formater.parse("23/03/2019").getTime()));
		funcionario2.setFuncao(funcao2);
		funcionario2.setEndereco(endereco2);
		funcionario2.setFormacaoAcademica(formacaoAcademica2);
		funcionario2.setDepartamento(departamento3);
		funcionario2.addPerfil(Perfil.FUNCIONARIO);
		
		FuncionarioSkill funcionarioSkill21 =  new FuncionarioSkill(funcionario2,skill1, "Básico");
		FuncionarioSkill funcionarioSkill22 =  new FuncionarioSkill(funcionario2,skill2, "Básico");
		FuncionarioSkill funcionarioSkill23 =  new FuncionarioSkill(funcionario2,skill5, "Básico");	
		
		funcionario2.setFuncionarioSkills(Stream.of(funcionarioSkill21, funcionarioSkill22, funcionarioSkill23).collect(Collectors.toSet()));
		
		Funcionario funcionario3 =  new Funcionario("Donato", "Anjos", "58599410482", "2977269",  new Date(formater.parse("01/02/1984").getTime()) , "donato@gmail.com", "Superior Completo", "11920189324", "Funcionario extremamente competente", "M", "2136", pe.encode("1234"), new Date(formater.parse("23/03/2019").getTime()));
		funcionario3.setFuncao(funcao3);
		funcionario3.setEndereco(endereco3);
		funcionario2.setFormacaoAcademica(formacaoAcademica3);
		funcionario3.setDepartamento(departamento1);
		
		funcionarioRepository.save(Arrays.asList(funcionario1, funcionario2, funcionario3));
	}
	
}
