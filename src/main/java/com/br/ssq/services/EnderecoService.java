package com.br.ssq.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.br.ssq.models.Endereco;
import com.br.ssq.repositories.EnderecoRepository;
import com.br.ssq.services.exceptions.DataIntegrityException;
import com.br.ssq.services.exceptions.ObjectNotFoundException;

@Service
public class EnderecoService {

	@Autowired
	private EnderecoRepository repository;
	
	public Endereco find(Integer id) {		
		
		Endereco endereco = repository.findOne(id);
		
		if(endereco == null) {			
			throw new ObjectNotFoundException("Objeto com o ID "+ id +" não foi encontrado");			
		}
		
		return  endereco;
	}
	
	public Endereco insert(Endereco endereco) {	
		
		return  repository.save(endereco);
		
	}
	
	public Endereco update(Endereco endereco) {		
		find(endereco.getId());
		return repository.save(endereco);
	}
	
	public void delete(Integer id) {
		
		find(id);
		
		try {
			repository.delete(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel excluir Endereço com o ID "+ id +", pois possui funcionarios atrelados ao mesmo");
		}		
	}
	
	public List<Endereco> findAll(){
		return repository.findAll();
	}
	
	
	
}
