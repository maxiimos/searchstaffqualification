package com.br.ssq.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.br.ssq.models.Vaga;
import com.br.ssq.repositories.VagaRepository;
import com.br.ssq.services.exceptions.DataIntegrityException;
import com.br.ssq.services.exceptions.ObjectNotFoundException;

@Service
public class VagaService {

	@Autowired
	private VagaRepository repository;
	
	public Vaga find(Integer id) {		
		
		Vaga vaga = repository.findOne(id);
		
		if(vaga == null) {			
			throw new ObjectNotFoundException("Objeto com o ID "+ id +" não foi encontrado");			
		}
		
		return  repository.findOne(id);
	}
	
	public Vaga insert(Vaga vaga) {	
		vaga.setDataCriado(new Date());
		return  repository.save(vaga);
		
	}
	
	public Vaga update(Vaga vaga) {		
		find(vaga.getId());
		return repository.save(vaga);
	}
	
	public void delete(Integer id) {
		
		find(id);
		
		try {
			repository.delete(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel excluir vaga com o ID "+ id +", pois possui funcionarios atrelados ao mesmo");
		}		
	}
	
	public List<Vaga> findAll(){
		return repository.findAll();
	}
	
	
	
}
