package com.br.ssq.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.br.ssq.models.Departamento;
import com.br.ssq.models.Funcao;
import com.br.ssq.repositories.FuncaoRepository;
import com.br.ssq.services.exceptions.DataIntegrityException;
import com.br.ssq.services.exceptions.ObjectNotFoundException;

@Service
public class FuncaoService {

	@Autowired
	private FuncaoRepository repository;
	
	//**********  MÉTODOS CRUD **********
	public Funcao find(Integer id) {		
		
		Funcao funcao = repository.findOne(id);
		
		if(funcao == null) {			
			throw new ObjectNotFoundException("Objeto com o ID "+ id +" não foi encontrado");			
		}
		
		return  repository.findOne(id);
	}
	
	public Funcao insert(Funcao funcao) {	
		
		return  repository.save(funcao);
		
	}
	
	public Funcao update(Funcao funcao) {		
		
		updateFucao(find(funcao.getId()), funcao);
		return repository.save(funcao);
	
	}
	
	public void delete(Integer id) {
		
		find(id);
		
		try {
			repository.delete(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel excluir função com o ID "+ id +", pois possui objetos atrelados ao mesmo");
		}		
	}
	
	public List<Funcao> findAll(){
		
		List<Funcao> list = repository.findAll();
		
		if(list.isEmpty()) {
			throw new ObjectNotFoundException("Não existe função cadastradas no sistema");
		}
		
		return list;
	}
	
	
	public void updateFucao(Funcao objOld, Funcao objNew) {
		
		if(objNew.getDepartamento()  == null) {
			objNew.setDepartamento(objOld.getDepartamento());
		} 
		
	}	
	
	
	//**********  MÉTODOS AUXILIARES **********
	
	public List<Funcao> findByDepartamento(Integer idDpeartamento){
		
		List<Funcao> list = repository.findDepartamento(idDpeartamento); 
		
		if(list.isEmpty()) {
			throw new ObjectNotFoundException("Não existe funções atreladas ao departamento");
		}
		
		return list ;
	}
	
	public Departamento getDepartamento(Integer idFuncao) {
		
		Departamento departamento = (find(idFuncao)).getDepartamento();
		
		if(departamento == null) {			
			throw new ObjectNotFoundException("Não existe departamento atrelado a função com o id "+ idFuncao );			
		}
		
		return departamento;
	}
	
}
