package com.br.ssq.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.br.ssq.dto.DepartamentoDTO;
import com.br.ssq.models.Departamento;
import com.br.ssq.models.Empresa;
import com.br.ssq.repositories.EmpresaRepository;
import com.br.ssq.services.exceptions.DataIntegrityException;
import com.br.ssq.services.exceptions.ObjectNotFoundException;

@Service
public class EmpresaService {

	@Autowired
	private EmpresaRepository repository;
	
	@Autowired
	private DepartamentoService departamentoService;
	
	public Empresa find(Integer id) {		
		
		Empresa empresa = repository.findOne(id);
		
		if(empresa == null) {			
			throw new ObjectNotFoundException("Objeto com o ID "+ id +" não foi encontrado");			
		}
		
		return  repository.findOne(id);
	}
	
	public Empresa insert(Empresa empresa) {	
		
		return  repository.save(empresa);
		
	}
	
	public Empresa update(Empresa empresa) {		
		find(empresa.getId());
		return repository.save(empresa);
	}
	
	public void delete(Integer id) {
		
		find(id);
		
		try {
			repository.delete(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel excluir empresa com o ID "+ id +", pois possui departamentos atrelados ao mesmo");
		}		
	}
	
	public List<Empresa> findAll(){
		
		List<Empresa> list = repository.findAll();
		
		if(list.isEmpty()) {
			throw new ObjectNotFoundException("Não existe empresas cadastradas no sistema");
		}
		
		return list;
	}
	
	public List<DepartamentoDTO> findDepartamentoByEmpresaId(Integer id) {
		
		find(id);
		List<Departamento> list = (departamentoService.findDepartamentoByEmpresaId(id));		
		List<DepartamentoDTO> listDTO = list.stream().map(obj -> new DepartamentoDTO(obj)).collect(Collectors.toList());
	
		
		return listDTO;
	}
	
}
