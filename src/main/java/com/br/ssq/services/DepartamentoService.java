package com.br.ssq.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.br.ssq.dto.FuncaoDTO;
import com.br.ssq.dto.FuncionarioDTO;
import com.br.ssq.models.Departamento;
import com.br.ssq.models.Empresa;
import com.br.ssq.models.Funcao;
import com.br.ssq.models.Funcionario;
import com.br.ssq.repositories.DepartamentoRepository;
import com.br.ssq.services.exceptions.DataIntegrityException;
import com.br.ssq.services.exceptions.ObjectNotFoundException;

@Service
public class DepartamentoService {

	@Autowired
	private DepartamentoRepository repository;
	
	@Autowired
	private FuncionarioService funcionarioService;
	
	@Autowired
	private FuncaoService funcaoService;
	
	//**********  MÉTODOS CRUD **********
	
	public Departamento find(Integer id) {		
		
		Departamento departamento = repository.findOne(id);
		
		if(departamento == null) {			
			throw new ObjectNotFoundException("Objeto com o ID "+ id +" não foi encontrado");			
		}
		
		return  repository.findOne(id);
	}
	
	public Departamento insert(Departamento departamento) {	
		return  repository.save(departamento);
		
	}
	
	public Departamento update(Departamento departamento) {		
		
		updateDepartamento(find(departamento.getId()),departamento);
		return repository.save(departamento);
	}
	
	public void delete(Integer id) {
		
		find(id);
		
		try {
			repository.delete(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel excluir departamento com o ID "+ id +", pois possui funcionarios atrelados ao mesmo");
		}		
	}
	
	public List<Departamento> findAll(){
		List<Departamento> list = repository.findAll();
		
		if(list.isEmpty()) {
			throw new ObjectNotFoundException("Não existe departamentos cadastrados no sistema");
		}
		
		return list;
	}
	
	public void updateDepartamento(Departamento objOld, Departamento objNew) {
		
		if(objNew.getEmpresa()  == null) {
			objNew.setEmpresa(objOld.getEmpresa());
		} 
		
	}	
	
	//**********  MÉTODOS AUXILIARES **********
	
	public List<Departamento> findDepartamentoByEmpresaId(Integer id){
		
		List<Departamento> list = repository.findDepartamento(id);
		
		if(list.isEmpty()) {
			throw new ObjectNotFoundException("Não existe departamentos cadastrados na empresa com o id: " + id);
		}
		
		return repository.findDepartamento(id);
	
	}
	
	public List<FuncionarioDTO> findFuncionariosByDepartamentoId(Integer id){
		
		List<Funcionario> list = funcionarioService.findFuncionariosByDepartamentoId(id);
		List<FuncionarioDTO> listDTO = list.stream().map(obj -> new FuncionarioDTO(obj)).collect(Collectors.toList());
		return listDTO;
		
	}
	
	public List<FuncaoDTO> findFuncaoByDepartamentoId(Integer id){		
		
		List<Funcao> list = funcaoService.findByDepartamento(id);
		List<FuncaoDTO> listDTO = list.stream().map(obj -> new FuncaoDTO(obj)).collect(Collectors.toList());
		return listDTO;
		
	}
	
	public Empresa getEmpresa(Integer id) {
		
		Empresa empresa = (find(id)).getEmpresa();
		
		if(empresa == null) {			
			throw new ObjectNotFoundException("Não existe Empresa atrelada ao departamento com o id "+ id );			
		}
		
		return empresa;
	}

	
}
