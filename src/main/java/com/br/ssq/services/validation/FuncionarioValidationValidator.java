package com.br.ssq.services.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerMapping;

import com.br.ssq.models.Funcionario;
import com.br.ssq.repositories.FuncionarioRepository;
import com.br.ssq.resources.exception.FieldMessage;
import com.br.ssq.services.validation.utils.Utils;

public class FuncionarioValidationValidator implements ConstraintValidator<FuncionarioValidation, Funcionario> {
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private FuncionarioRepository repository;
	
	@Override
	public void initialize(FuncionarioValidation ann) {
	}

	@Override
	public boolean isValid(Funcionario funcionario, ConstraintValidatorContext context) {
	
		List<FieldMessage> list = new ArrayList<>();
		Integer id = 0;
		Funcionario aux = null;
		
		try {
			
			@SuppressWarnings("unchecked")
			Map<String, String> map = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
			id = Integer.parseInt(map.get("id"));
		
		} catch (Exception e) {

		}
		
		if(!Utils.isValidCPF(funcionario.getCpf())) {
			list.add(new FieldMessage("cpf", "CPF Inválido"));
		}
		
		
		try {
			aux = repository.findByCpf(funcionario.getCpf());
		} catch (Exception e) {

		}
	
		if(id == 0 ) {
			if(aux != null) {
				
				list.add(new FieldMessage("cpf", "Funcionário com o CPF: " + funcionario.getCpf() +" já existente na base de dados"));
				
			}			
		} else if (aux != null && aux.getId() != id) {
			
				list.add(new FieldMessage("cpf", "Funcionário com o CPF: " + funcionario.getCpf() +" já existente na base de dados"));
				
		}
		
		
		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		return list.isEmpty();
	}
}


