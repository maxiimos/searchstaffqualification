package com.br.ssq.services.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerMapping;

import com.br.ssq.models.Empresa;
import com.br.ssq.repositories.EmpresaRepository;
import com.br.ssq.resources.exception.FieldMessage;
import com.br.ssq.services.validation.utils.Utils;

public class EmpresaValidationValidator implements ConstraintValidator<EmpresaValidation, Empresa> {
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private EmpresaRepository repository;
	
	@Override
	public void initialize(EmpresaValidation ann) {
	}

	@Override
	public boolean isValid(Empresa empresa, ConstraintValidatorContext context) {
	
		List<FieldMessage> list = new ArrayList<>();
		Integer id = 0;
		Empresa aux = null;
		
		try {
			
			@SuppressWarnings("unchecked")
			Map<String, String> map = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
			id = Integer.parseInt(map.get("id"));
		
		} catch (Exception e) {

		}
		
		if(!Utils.isValidCNPJ(empresa.getCnpj())) {
			list.add(new FieldMessage("cnpj", "CNPJ Inválido"));
		}
		
		
		try {
			aux = repository.findByCnpj(empresa.getCnpj());
		} catch (Exception e) {

		}
	
		if(id == 0 ) {
			if(aux != null) {
				
				list.add(new FieldMessage("cnpj", "Empresa com o CNPJ: " + empresa.getCnpj() +" já existente na base de dados"));
				
			}		
			//CHECK PARA SE CASO FOR UPDATE AO INVÉS DE INSERT
		} else if (aux != null && aux.getId() != id) {
			
				list.add(new FieldMessage("cnpj", "Empresa com o CNPJ: " + empresa.getCnpj() +" já existente na base de dados"));
				
		}
		
		
		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		return list.isEmpty();
	}
	
}


