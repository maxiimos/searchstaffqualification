package com.br.ssq.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.br.ssq.models.FormacaoAcademica;
import com.br.ssq.repositories.FormacaoAcademicaRepository;
import com.br.ssq.services.exceptions.DataIntegrityException;
import com.br.ssq.services.exceptions.ObjectNotFoundException;

@Service
public class FormacaoAcademicaService {

	@Autowired
	private FormacaoAcademicaRepository repository;
	
	public FormacaoAcademica find(Integer id) {		
		
		FormacaoAcademica formacaoAcademica = repository.findOne(id);
		
		if(formacaoAcademica == null) {			
			throw new ObjectNotFoundException("Objeto com o ID "+ id +" não foi encontrado");			
		}
		
		return  repository.findOne(id);
	}
	
	public FormacaoAcademica insert(FormacaoAcademica formacaoAcademica) {	
		
		return  repository.save(formacaoAcademica);
		
	}
	
	public FormacaoAcademica update(FormacaoAcademica formacaoAcademica) {		
		find(formacaoAcademica.getId());
		return repository.save(formacaoAcademica);
	}
	
	public void delete(Integer id) {
		
		find(id);
		
		try {
			repository.delete(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel excluir formacao academica com o ID "+ id +", pois possui funcionarios atrelados ao mesmo");
		}		
	}
	
	public List<FormacaoAcademica> findAll(){
		return repository.findAll();
	}
	
	
	
}
