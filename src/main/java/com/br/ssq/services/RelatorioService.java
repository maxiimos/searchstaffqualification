package com.br.ssq.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.ssq.dto.SkillNameCountDTO;
import com.br.ssq.models.relatorios.RelatorioCount;
import com.br.ssq.models.relatorios.SkillNameAndCount;
import com.br.ssq.repositories.FuncionarioRepository;
import com.br.ssq.repositories.SkillRepository;

@Service
public class RelatorioService {
	
	@Autowired
	private FuncionarioRepository funcionarioRepository;
	
	@Autowired
	private SkillRepository skillRepository;
	
	public RelatorioCount contFuncionario() {
		
		List<SkillNameAndCount> list = skillRepository.skillAndQtd();	
		List<SkillNameCountDTO> listDTO = list.stream().map(obj -> new SkillNameCountDTO(obj)).collect(Collectors.toList());
				
		
		
		RelatorioCount relatorio = new RelatorioCount(
				funcionarioRepository.count(),
				funcionarioRepository.contNewFuncionarios(),
				skillRepository.count(),
				skillRepository.contNewSkill(),
				listDTO);
		
		return relatorio;
	}

}
