package com.br.ssq.services;

import java.awt.image.BufferedImage;
import java.net.URI;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.br.ssq.models.Endereco;
import com.br.ssq.models.FormacaoAcademica;
import com.br.ssq.models.Funcionario;
import com.br.ssq.models.FuncionarioSkill;
import com.br.ssq.repositories.FuncionarioRepository;
import com.br.ssq.repositories.FuncionarioSkillRepository;
import com.br.ssq.security.UserSS;
import com.br.ssq.services.exceptions.AuthorizationException;
import com.br.ssq.services.exceptions.DataIntegrityException;
import com.br.ssq.services.exceptions.ObjectNotFoundException;
import com.br.ssq.utils.FuncionarioSpecificationBuilder;

@Service
public class FuncionarioService {

	@Autowired
	private FuncionarioRepository repository;

	@Autowired
	private FuncionarioSkillRepository funcionarioSkillRepository;

	@Autowired
	private EnderecoService enderecoService;

	@Autowired
	private FormacaoAcademicaService formacaoAcademicaService;
	
	@Autowired
	private BCryptPasswordEncoder pe;
	
	@Autowired
	private S3Service s3Service;
	
	@Autowired
	private ImageService imageService;
	
	@Value("${img.prefix.client.profile}")
	private String prefix;

	@Value("${img.profile.size}")
	private Integer size;
	
	/**************** CRUD FUNCIONARIO ****************/

	public Funcionario find(Integer id) {

		UserSS user = UserService.authenticated();
		if(user == null || !user.hasRole() && !id.equals(user.getId())) {
			throw new AuthorizationException("Acesso Negado");
		}
		
		Funcionario funcionario = repository.findOne(id);

		if (funcionario == null) {
			throw new ObjectNotFoundException("Objeto com o ID " + id + " não foi encontrado");
		}

		return funcionario;
	}

	public Funcionario insert(Funcionario funcionario) {

		funcionario.setDataCriado(new Date());
		funcionario.setSenha(pe.encode(funcionario.getCpf()));
		return repository.save(funcionario);

	}

	public Funcionario update(Funcionario funcionario) {
		
		updateFuncionario(find(funcionario.getId()), funcionario);
		return repository.save(funcionario);

	}

	public void delete(Integer id) {

		find(id);

		try {
			repository.delete(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException(
					"Não é possivel excluir funcionario com o ID " + id + ", pois possui objetos atrelados ao mesmo");
		}
	}

	public List<Funcionario> findAll() {

		List<Funcionario> list = repository.findAll();

		if (list.isEmpty()) {
			throw new ObjectNotFoundException("Não existe funcionarios cadastrados no sistema");
		}

		return list;
	}
	
	
	public Funcionario findByEmail(String email) {
		
		Funcionario funcionario = repository.findByEmail(email);

		if (funcionario == null) {
			throw new ObjectNotFoundException("Funcionario com o email " + email + " não foi encontrado");
		}

		return funcionario;
	}

	public void updateFuncionario(Funcionario objOld, Funcionario objNew) {

		if (objNew.getEndereco() == null) {
			objNew.setEndereco(objOld.getEndereco());
		}
		if (objNew.getDepartamento() == null) {
			objNew.setDepartamento(objOld.getDepartamento());
		}
		if (objNew.getFuncao() == null) {
			objNew.setFuncao(objOld.getFuncao());
		}
		if (objNew.getFormacaoAcademica() == null) {
			objNew.setFormacaoAcademica(objOld.getFormacaoAcademica());
		}
		if (objNew.getFuncionarioSkills().isEmpty()) {
			objNew.setFuncionarioSkills(objOld.getFuncionarioSkills());
		}

	}

	public List<Funcionario> findCustomURL(String search) {
		
		FuncionarioSpecificationBuilder builder = new FuncionarioSpecificationBuilder();
		Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
		Matcher matcher = pattern.matcher(search + ",");

		while (matcher.find()) {
			builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
		}

		Specification<Funcionario> spec = builder.build();

		return repository.findAll(spec);
	}

	/**************** CRUD OBJETOS RELACIONAIS ****************/

	public URI uploadProfilePicture(Integer id,MultipartFile multipartFile) {
		
		Funcionario funcionario = find(id);
	
		BufferedImage jpgImage = imageService.getJpgImageFromFile(multipartFile);
		
		jpgImage = imageService.cropSquare(jpgImage);
		jpgImage = imageService.resize(jpgImage, size);
		
		String fileName = prefix+ funcionario.getId() + ".jpg";
		
		return s3Service.uploadFile(imageService.getInputStream(jpgImage, "jpg"), fileName, "image");
	}
	
	/******* CRUD Endereco *******/
	public Endereco findEndereco(Integer id) {

		Endereco endereco = (find(id)).getEndereco();

		if (endereco == null) {
			throw new ObjectNotFoundException("Não existe endereco atrelado ao funcionario com id " + id);
		}

		return endereco;

	}

	public Endereco updateEndereco(Integer id, Endereco endereco) {
		Funcionario funcionario = find(id);

		if (funcionario.getEndereco() == null) {
			funcionario.setEndereco(endereco);
			return (update(funcionario)).getEndereco();
		} else {
			endereco.setId(funcionario.getEndereco().getId());
			return enderecoService.update(endereco);
		}

	}

	/******* CRUD Formação Academica *******/
	public FormacaoAcademica findFormacao(Integer id) {

		FormacaoAcademica formacao = (find(id)).getFormacaoAcademica();

		if (formacao == null) {
			throw new ObjectNotFoundException("Não existe formação academica atrelado ao funcionario com id " + id);
		}

		return formacao;
	}

	public FormacaoAcademica updateFormacao(Integer id, FormacaoAcademica formacao) {

		Funcionario funcionario = find(id);

		if (funcionario.getFormacaoAcademica() == null) {
			funcionario.setFormacaoAcademica(formacao);
			return (update(funcionario)).getFormacaoAcademica();
		} else {
			formacao.setId(funcionario.getFormacaoAcademica().getId());
			return formacaoAcademicaService.update(formacao);
		}

	}

	/******* CRUD Skill *******/
	public List<FuncionarioSkill> findSkill(Integer id) {

		List<FuncionarioSkill> list = funcionarioSkillRepository.findByFuncionarioId(id);
		
		if (list.isEmpty()) {

			throw new ObjectNotFoundException("Não existe skills atreladas ao funcionario com o id " + id);
		}

		return list;
	}


	public Set<FuncionarioSkill> updateSkill(Integer id, Set<FuncionarioSkill> skills) {

		Funcionario funcionario = find(id);

		Iterator<FuncionarioSkill> it = skills.iterator();
		while (it.hasNext()) {
			FuncionarioSkill funcionarioSkill = it.next();
			funcionarioSkill.setFuncionario(funcionario);			
		}
		
		funcionario.getFuncionarioSkills().clear();
		funcionario.getFuncionarioSkills().addAll(skills);
	
		funcionario = repository.save(funcionario);

		return funcionario.getFuncionarioSkills();

	}

	
	/**************** MÉTODOS AUXILIARES ****************/

	public List<Funcionario> findFuncionariosByDepartamentoId(Integer id) {

		List<Funcionario> list = repository.findByDepartamentoId(id);

		if (list.isEmpty()) {

			throw new ObjectNotFoundException("Não existe funcionários atrelado ao departamento com o id " + id);
		}

		return list;
	}


}
