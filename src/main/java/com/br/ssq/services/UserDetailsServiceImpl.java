package com.br.ssq.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.br.ssq.models.Funcionario;
import com.br.ssq.repositories.FuncionarioRepository;
import com.br.ssq.security.UserSS;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	private FuncionarioRepository repository;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		
		Funcionario funcionario = repository.findByEmail(email);
		
		if(funcionario == null) {
			throw new UsernameNotFoundException(email);
		} 
		
		return new UserSS(funcionario.getId(), funcionario.getEmail(), funcionario.getSenha(), funcionario.getPerfis());
	}

}
