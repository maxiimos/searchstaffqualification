package com.br.ssq.services;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.br.ssq.models.Funcionario;
import com.br.ssq.repositories.FuncionarioRepository;
import com.br.ssq.services.exceptions.ObjectNotFoundException;

@Service
public class AuthService {

	@Autowired
	private FuncionarioRepository funcionarioRepository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	private Random random = new Random();
	
	public void sendPassword(String email) {
		
		Funcionario funcionario = funcionarioRepository.findByEmail(email);
		
		if(funcionario == null) {
			throw new ObjectNotFoundException("E-mail não encontrado");
		}
		
		String newPass = newPassword();
		funcionario.setSenha(passwordEncoder.encode(newPass));
		System.out.println("Senha: " + newPass);
		funcionarioRepository.save(funcionario);
	}

	private String newPassword() {
		
		char[] vet = new char[10];
		for(int i=0; i<10 ; i++) {
			vet[i] = randomChar();
		}
		
		return new String(vet);
	}

	private char randomChar() {
		int opt = random.nextInt(3);
		
		if(opt == 0) {//Gera digito
			return (char) (random.nextInt(10) + 48);
		} else if (opt ==1) {//Gera letra maiuscula
			return (char) (random.nextInt(26) + 65);
		} else {// Gera letra minuscula
			return (char) (random.nextInt(26) + 97);
		}
	}
	
}
