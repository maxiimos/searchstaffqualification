package com.br.ssq.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.ssq.models.Endereco;
import com.br.ssq.services.EnderecoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/enderecos")
@Api(tags={"Endereço"}, description="Serviços endereço", value="/endereco")
public class EnderecoResource {
	
	@Autowired
	private EnderecoService service;
	
	@ApiOperation(value = "Adicionar Endereço", response=Endereco.class)
	@RequestMapping(method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<?> insert(@RequestBody @Valid Endereco endereco) {
		
		return new ResponseEntity<Endereco>(service.insert(endereco), HttpStatus.CREATED);
		
	}
	
	@ApiOperation(value = "Atualizar Endereço", response=Endereco.class)
	@RequestMapping(value="/{id}", method=RequestMethod.PUT,produces = "application/json")
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody @Valid  Endereco endereco) {
		
		endereco.setId(id);
		return new ResponseEntity<Endereco>(service.update(endereco),HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Deletar Endereço", response=Endereco.class)
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE,produces = "application/json")
	public ResponseEntity<?> delete(@PathVariable("id") int id) {		

		service.delete(id);	
		return new ResponseEntity<Endereco>(HttpStatus.NO_CONTENT);
		
	}
	
	@ApiOperation(value = "Pegar Endereço", response=Endereco.class)
	@RequestMapping(value="/{id}", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> find(@PathVariable("id") int id) {		
		
		return new ResponseEntity<Endereco>(service.find(id), HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Listar Endereço", response=Endereco.class)
	@RequestMapping(method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> list() {		
		
		return new ResponseEntity<List<Endereco>>(service.findAll(), HttpStatus.OK);
	
	}
	
}
