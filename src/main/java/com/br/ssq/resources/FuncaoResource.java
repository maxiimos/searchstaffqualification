package com.br.ssq.resources;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.ssq.dto.DepartamentoDTO;
import com.br.ssq.dto.FuncaoDTO;
import com.br.ssq.models.Funcao;
import com.br.ssq.services.FuncaoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/funcoes")
@Api(tags={"Função"}, description="Serviços Função", value="/funcao")
@CrossOrigin("${origem-permitida}")
public class FuncaoResource {
	
	@Autowired
	private FuncaoService service;
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_RH') or hasRole('ROLE_GESTOR')")
	@ApiOperation(value = "Adicionar Função", response=FuncaoDTO.class)
	@RequestMapping(method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<?> insert(@RequestBody @Valid Funcao funcao) {
		
		Funcao insert = service.insert(funcao);		
		return new ResponseEntity<FuncaoDTO>(new FuncaoDTO(insert),HttpStatus.CREATED);
		
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_RH') or hasRole('ROLE_GESTOR')")
	@ApiOperation(value = "Atualizar Função", response=FuncaoDTO.class)
	@RequestMapping(value="/{id}", method=RequestMethod.PUT,produces = "application/json")
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody @Valid Funcao funcao) {
		
		funcao.setId(id);
		return new ResponseEntity<FuncaoDTO>(new FuncaoDTO(service.update(funcao)), HttpStatus.OK);
		
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_RH') or hasRole(ROLE_GESTOR)")
	@ApiOperation(value = "Deletar Função", response=FuncaoDTO.class)
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE,produces = "application/json")
	public ResponseEntity<?> delete(@PathVariable("id") int id) {
		
		service.delete(id);	
		return new ResponseEntity<Funcao>(HttpStatus.NO_CONTENT);
	
	}
	
	@ApiOperation(value = "Pegar Função", response=FuncaoDTO.class)
	@RequestMapping(value="/{id}", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> find(@PathVariable("id") int id) {
		
		return new ResponseEntity<FuncaoDTO>(new FuncaoDTO(service.find(id)), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Listar Função", response=FuncaoDTO.class)
	@RequestMapping(method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> listar() {

		List<Funcao> list = service.findAll();
		List<FuncaoDTO> listDTO = list.stream().map(obj -> new FuncaoDTO(obj)).collect(Collectors.toList());
		
		return new ResponseEntity<List<FuncaoDTO>>(listDTO, HttpStatus.OK);

	}
	
	@ApiOperation(value = "Pegar departamento da função", response=DepartamentoDTO.class)
	@RequestMapping(value="/{id}/departamentos", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> findDepartamento(@PathVariable("id") int id) {
		
		return new ResponseEntity<DepartamentoDTO>(new DepartamentoDTO(service.getDepartamento(id)), HttpStatus.OK);
	
	}	
	
}
