package com.br.ssq.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.ssq.models.FormacaoAcademica;
import com.br.ssq.services.FormacaoAcademicaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("/formacoes")
@Api(tags={"Formacao Academica"}, description="Serviços Formação Acadêmica", value="/formacao")
public class FormacaoAcademicaResource {

	//private static final Logger LOGGER = LoggerFactory.getLogger(FormacaoAcademicaResource.class);

	@Autowired
	private FormacaoAcademicaService service;
	
	@ApiOperation(value = "Adicionar Formação", response=FormacaoAcademica.class)
	@RequestMapping(method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<?> insert(@RequestBody @Valid FormacaoAcademica formacaoAcademica) {
		
		return new ResponseEntity<FormacaoAcademica>(service.insert(formacaoAcademica), HttpStatus.CREATED);
		
	}
	
	@ApiOperation(value = "Atualizar Formação", response=FormacaoAcademica.class)
	@RequestMapping(value="/{id}", method=RequestMethod.PUT,produces = "application/json")
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody @Valid FormacaoAcademica formacaoAcademica) {
		
		formacaoAcademica.setId(id);
		return new ResponseEntity<FormacaoAcademica>(service.update(formacaoAcademica), HttpStatus.OK);
	
	}
	
	@ApiOperation(value = "Deletar Formação", response=FormacaoAcademica.class)
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE,produces = "application/json")
	public ResponseEntity<?> delete(@PathVariable("id") int id) {
		
		service.delete(id);	
		return new ResponseEntity<FormacaoAcademica>(HttpStatus.NO_CONTENT);
	
	}
	
	@ApiOperation(value = "Pegar Formação", response=FormacaoAcademica.class)
	@RequestMapping(value="/{id}", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> find(@PathVariable("id") int id) {
	
		return new ResponseEntity<FormacaoAcademica>(service.find(id), HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Listar Formação", response=FormacaoAcademica.class)
	@RequestMapping(method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> list() {
		
		return new ResponseEntity<List<FormacaoAcademica>>(service.findAll(), HttpStatus.OK);

	}
	
	
}
