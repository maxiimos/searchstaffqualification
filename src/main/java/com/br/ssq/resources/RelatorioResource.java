package com.br.ssq.resources;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.ssq.models.relatorios.RelatorioCount;
import com.br.ssq.services.RelatorioService;

@RestController
@RequestMapping("/relatorios")
//@Api(tags={"Função"}, description="Serviços Função", value="/funcao")
@CrossOrigin("${origem-permitida}")
public class RelatorioResource {

	@Autowired
	private RelatorioService service;
	
	
	@RequestMapping(method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> get() {
		
		return new ResponseEntity<RelatorioCount>(service.contFuncionario(), HttpStatus.ACCEPTED);
		
	}
	
	
}
