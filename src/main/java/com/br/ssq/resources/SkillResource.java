package com.br.ssq.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.ssq.models.Skill;
import com.br.ssq.models.SkillAprovacao;
import com.br.ssq.services.SkillService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/skills")
@Api(tags={"Skill"}, description="Serviços Skill", value="/skill")
@CrossOrigin("${origem-permitida}")
public class SkillResource {

	@Autowired
	private SkillService service;
	
	@ApiOperation(value = "Adicionar Skill", response=Skill.class)
	@RequestMapping(method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<?> insert(@RequestBody @Valid Skill skill) {
		
		return new ResponseEntity<>(service.insereSkillAprovacao(skill), HttpStatus.CREATED);
		
	}
	
	@ApiOperation(value = "Atualizar Skill", response=Skill.class)
	@RequestMapping(value="/{id}", method=RequestMethod.PUT,produces = "application/json")
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody @Valid Skill skill) {
	
		return new ResponseEntity<Skill>(service.update(skill), HttpStatus.CREATED);
		
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_RH') or hasRole(ROLE_GESTOR)")
	@ApiOperation(value = "Deletar Skill", response=Skill.class)
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE,produces = "application/json")
	public ResponseEntity<?> delete(@PathVariable("id") int id) {		

		service.delete(id);
		
		return new ResponseEntity<Skill>(HttpStatus.NO_CONTENT);
		
	}
	
	@ApiOperation(value = "Pegar Skill", response=Skill.class)
	@RequestMapping(value="/{id}", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> find(@PathVariable("id") int id) {		
				
		return new ResponseEntity<Skill>(service.find(id), HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Listar Skill", response=Skill.class)
	@RequestMapping(method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> list() {		
	
		return new ResponseEntity<List<Skill>>(service.findAll(), HttpStatus.OK);
		
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_RH') or hasRole('ROLE_GESTOR')")
	@ApiOperation(value = "Listar Skill para aprovação", response=SkillAprovacao.class)
	@RequestMapping(value="aprovacao",method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> listSkillAprovacao() {		
	
		return new ResponseEntity<List<SkillAprovacao>>(service.findAllAprovacao(), HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Adicionar Skill", response=SkillAprovacao.class)
	@RequestMapping(value="aprovacao",method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<?> insertSkill(@RequestBody @Valid SkillAprovacao skill) {		
	
		System.out.println("passou");
		return new ResponseEntity<List<SkillAprovacao>>(service.insereSkill(skill), HttpStatus.OK);
		
	}
	
}
