package com.br.ssq.resources;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.ssq.dto.DepartamentoDTO;
import com.br.ssq.dto.FuncaoDTO;
import com.br.ssq.dto.FuncionarioDTO;
import com.br.ssq.jsoncustons.DepartamentoJsonCustom;
import com.br.ssq.models.Departamento;
import com.br.ssq.models.Empresa;
import com.br.ssq.services.DepartamentoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/departamentos")
@Api(tags={"Departamento"}, description="Serviços departamento", value="/departamento")
@CrossOrigin("${origem-permitida}")
public class DepartamentoResource {
	
	//private static final Logger LOGGER = LoggerFactory.getLogger(DepartamentoResource.class);
	
	@Autowired
	private DepartamentoService service;

	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_GESTOR') or hasRole('ROLE_RH')")
	@ApiOperation(value = "Adicionar Departamento", response=Departamento.class)
	@RequestMapping(method=RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> insert(@RequestBody @Valid Departamento departamento) {
		
		return new ResponseEntity<Departamento>(service.insert(departamento),HttpStatus.CREATED);
		
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_GESTOR') or hasRole('ROLE_RH')")
	@ApiOperation(value = "Atualizar Departamento", response=DepartamentoJsonCustom.class)
	@RequestMapping(value="/{id}", method=RequestMethod.PUT, produces = "application/json")	
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody @Valid Departamento departamento) {
		
		departamento.setId(id);
		return new ResponseEntity<DepartamentoDTO>(new DepartamentoDTO(service.update(departamento)), HttpStatus.OK);
		
	}
	
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_GESTOR') or hasRole('ROLE_RH')")
	@ApiOperation(value = "Deletar Departamento", response=Departamento.class)
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<?> delete(@PathVariable("id") int id) {	
		
		service.delete(id);		
		return new ResponseEntity<Departamento>(HttpStatus.NO_CONTENT);
		
	}
	
	@ApiOperation(value = "Pegar Departamento", response=Departamento.class)
	@RequestMapping(value="/{id}", method=RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> find(@PathVariable("id") int id) {		
		
		return new ResponseEntity<Departamento>(service.find(id), HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Listar Departamentos", response=Departamento.class)
	@RequestMapping(method=RequestMethod.GET , produces = "application/json")
	public ResponseEntity<?> list(){
		
		List<Departamento> list = service.findAll();
		List<DepartamentoDTO> listDTO = list.stream().map(obj -> new DepartamentoDTO(obj)).collect(Collectors.toList());
		
		return new ResponseEntity<List<DepartamentoDTO>>(listDTO, HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Pegar Empresa atrelada ao departamento", response=Empresa.class)
	@RequestMapping(value="/{id}/empresas", method=RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> findEmpresa(@PathVariable("id") int id) {		
		
		return new ResponseEntity<Empresa>(service.getEmpresa(id), HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Pegar funcionarios atrelados ao departamento", response=Departamento.class)
	@RequestMapping(value="/{id}/funcionarios", method=RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> findFuncionarios(@PathVariable("id") int id) {		
		
		return new ResponseEntity<List<FuncionarioDTO>>(service.findFuncionariosByDepartamentoId(id), HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Pegar funções atreladas ao departamento", response=Departamento.class)
	@RequestMapping(value="/{id}/funcoes", method=RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> findFuncoes(@PathVariable("id") int id) {		
	
		return new ResponseEntity<List<FuncaoDTO>>(service.findFuncaoByDepartamentoId(id), HttpStatus.OK);
		
	}
	
}
