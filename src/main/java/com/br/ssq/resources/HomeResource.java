package com.br.ssq.resources;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@RestController
public class HomeResource {

	@RequestMapping(value="/")
	public RedirectView index() {
		return new RedirectView("swagger-ui.html#");
	}
	
}
