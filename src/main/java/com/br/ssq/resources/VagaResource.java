package com.br.ssq.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.ssq.models.Skill;
import com.br.ssq.models.Vaga;
import com.br.ssq.services.VagaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags={"Vagas"}, description="Serviços Vagas", value="/vaga")
@RestController
@RequestMapping(value="/vagas")
public class VagaResource {
 
	
	@Autowired
	private VagaService service;
	
	@ApiOperation(value = "Adicionar vaga", response=Skill.class)
	@RequestMapping(method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<?> insert(@RequestBody @Valid Vaga vaga) {
		
		return new ResponseEntity<Vaga>(service.insert(vaga), HttpStatus.CREATED);
		
	}
	
	@ApiOperation(value = "Atualizar Vaga", response=Skill.class)
	@RequestMapping(value="/{id}", method=RequestMethod.PUT,produces = "application/json")
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody @Valid Vaga vaga) {
		
		vaga.setId(id);
		return new ResponseEntity<Vaga>(service.update(vaga), HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Deletar Vaga", response=Skill.class)
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE,produces = "application/json")
	public ResponseEntity<?> delete(@PathVariable("id") int id) {		
		
		service.delete(id);
		return new ResponseEntity<Vaga>(HttpStatus.NO_CONTENT);
		
	}
	
	@ApiOperation(value = "Pegar Vaga", response=Skill.class)
	@RequestMapping(value="/{id}", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> find(@PathVariable("id") int id) {		
		
		return new ResponseEntity<Vaga>(service.find(id), HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Listar Vaga", response=Skill.class)
	@RequestMapping(method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> list() {		
		
		return new ResponseEntity<List<Vaga>>(service.findAll(), HttpStatus.OK);
	}
	
}
