package com.br.ssq.resources;

import java.net.URI;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.br.ssq.models.Endereco;
import com.br.ssq.models.FormacaoAcademica;
import com.br.ssq.models.Funcionario;
import com.br.ssq.models.FuncionarioSkill;
import com.br.ssq.services.FuncionarioService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/funcionarios")
@Api(tags={"Funcionario"}, description="Serviços Funcionario", value="/funcionario")
@CrossOrigin("${origem-permitida}")
public class FuncionarioResource {

	//private static final Logger LOGGER = LoggerFactory.getLogger(FuncionarioResource.class);
	
	@Autowired
	private FuncionarioService service;
	
	/* Metodos CRUD */
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_RH') or hasRole(ROLE_GESTOR)")
	@ApiOperation(value = "Adicionar Funcionario", response=Funcionario.class)
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<?> insert(@RequestBody @Valid Funcionario funcionario ) {
		
		return new ResponseEntity<Funcionario>(service.insert(funcionario),HttpStatus.CREATED);
	}

	@ApiOperation(value = "Atualizar Funcionario", response=Funcionario.class)
	@RequestMapping(value="/{id}", method=RequestMethod.PUT,produces = "application/json")
	public ResponseEntity<?> update(@PathVariable("id") int id , @RequestBody @Valid Funcionario funcionario ) {
		
		funcionario.setId(id);
		
		return new ResponseEntity<Funcionario>(service.update(funcionario), HttpStatus.OK);

	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_RH') or hasRole(ROLE_GESTOR)")
	@ApiOperation(value = "Deletar Funcionario", response=Funcionario.class)
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE,produces = "application/json")
	public ResponseEntity<?> delete(@PathVariable("id") int id) {
		
		service.delete(id);		
		return new ResponseEntity<Funcionario>(HttpStatus.NO_CONTENT);
	}
	
	@ApiOperation(value = "Pegar Funcionario", response=Funcionario.class)
	@RequestMapping(value="/{id}", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> find(@PathVariable("id") int id) {
		
		return new ResponseEntity<Funcionario>(service.find(id), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Listar Funcionario", response=Funcionario.class)
	@RequestMapping(method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> list() {
			
		return new ResponseEntity<List<Funcionario>>(service.findAll(), HttpStatus.OK);

	}
	
	
	@ApiOperation(value = "Pegar Funcionario por email", response=Funcionario.class)
	@RequestMapping(value="/email",method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> findByEmail(@RequestParam(value="email", required=true) String email) {
		
		System.out.println("1");
		return new ResponseEntity<Funcionario>(service.findByEmail(email), HttpStatus.OK);
	}
	
	
	/**************** Pesquisa com parâmetro e valor na URL ****************/
	
	@ApiOperation(value = "Pesquisar Funcionario URL", response=Funcionario.class)
	@RequestMapping(value="/pesquisar",method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> findCustomURL(@RequestParam(value="search", required=true) String search){
		
			System.out.println("teste");
		
			return new ResponseEntity<List<Funcionario>>(service.findCustomURL(search), HttpStatus.OK);
	
	}
	
	/**************** CRUD OBJETOS RELACIONAL ****************/
	
	@RequestMapping(value="{id}/foto", method=RequestMethod.POST)
	public ResponseEntity<?> uploadProfilePicture(@PathVariable("id") Integer id, @RequestParam(value="file") MultipartFile multipartFile) {		
		return new ResponseEntity<URI>(service.uploadProfilePicture(id,multipartFile),HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Pegar endereço do funcionario", response=Endereco.class)
	@RequestMapping(value="/{id}/enderecos", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> findEndereco(@PathVariable("id") int id) {
		
		return new ResponseEntity<Endereco>(service.findEndereco(id), HttpStatus.OK);
	
	}
	
	@ApiOperation(value = "Atualizar endereço do funcionario", response=Endereco.class)
	@RequestMapping(value="/{id}/enderecos", method=RequestMethod.PUT,produces = "application/json")
	public ResponseEntity<?> updateEndereco(@PathVariable("id") int id, @RequestBody @Valid Endereco endereco) {
		
		return new ResponseEntity<Endereco>(service.updateEndereco(id, endereco), HttpStatus.OK);
	
	}
	
	
	@ApiOperation(value = "Pegar Formação Academica do funcionario", response=FormacaoAcademica.class)
	@RequestMapping(value="/{id}/formacoes", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> findFormacao(@PathVariable("id") int id) {
		
		return new ResponseEntity<FormacaoAcademica>(service.findFormacao(id), HttpStatus.OK);
	
	}
	
	@ApiOperation(value = "Atualizar Formação Academica do funcionario", response=FormacaoAcademica.class)
	@RequestMapping(value="/{id}/formacoes", method=RequestMethod.PUT,produces = "application/json")
	public ResponseEntity<?> updateFormacao(@PathVariable("id") int id, @RequestBody @Valid FormacaoAcademica formacao) {
		
		return new ResponseEntity<FormacaoAcademica>(service.updateFormacao(id, formacao), HttpStatus.OK);
	
	}
	
	@ApiOperation(value = "Pegar skills do funcionario", response=Funcionario.class)
	@RequestMapping(value="/{id}/skills", method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> findSkill(@PathVariable("id") int id) {
		
		return new ResponseEntity<List<FuncionarioSkill>>(service.findSkill(id), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Atualizas skills do funcionario", response=Funcionario.class)
	@RequestMapping(value="/{id}/skills", method=RequestMethod.PUT,produces = "application/json")
	public ResponseEntity<?> updateSkill(@PathVariable("id") int id, @RequestBody @Valid Set<FuncionarioSkill> skills) {
		
		return new ResponseEntity<Set<FuncionarioSkill>>(service.updateSkill(id, skills), HttpStatus.OK);
	}
	
}
