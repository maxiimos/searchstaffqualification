package com.br.ssq.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.ssq.dto.DepartamentoDTO;
import com.br.ssq.models.Departamento;
import com.br.ssq.models.Empresa;
import com.br.ssq.services.EmpresaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/empresas")
@Api(tags={"Empresa"}, description="Serviços empresa", value="/empresas")
@CrossOrigin("${origem-permitida}")
public class EmpresaResource {

//	private static final Logger LOGGER = LoggerFactory.getLogger(EmpresaResource.class);
	
	@Autowired
	private EmpresaService service;

	@ApiOperation(value = "Listar Empresa", response=Empresa.class)
	@RequestMapping(method=RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> list(){
		
		return new ResponseEntity<List<Empresa>>(service.findAll(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Pegar Empresa", response=Empresa.class)
	@RequestMapping(value="/{id}", method=RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> find(@PathVariable("id") int id) {
	
		return new ResponseEntity<Empresa>(service.find(id), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_RH')")
	@ApiOperation(value = "Adicionar Empresa", response=Empresa.class)
	@RequestMapping(method=RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> insert(@RequestBody @Valid Empresa empresa){
			
		return new ResponseEntity<Empresa>(service.insert(empresa),HttpStatus.CREATED);
	}
	
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_RH')")
	@ApiOperation(value = "Atualizar Empresa", response=Empresa.class)
	@RequestMapping(value="/{id}", method=RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody @Valid Empresa empresa) {
		
		empresa.setId(id);
		return new ResponseEntity<Empresa>(service.update(empresa),HttpStatus.OK);
		
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_RH')")
	@ApiOperation(value = "Deletar Empresa", response=Empresa.class)
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<?> delete(@PathVariable("id") int id) {
		
		service.delete(id);
		return new ResponseEntity<Empresa>(HttpStatus.NO_CONTENT);
	
	} 
	
	@ApiOperation(value = "Pegar Departamentos da Empresa", response=Departamento.class)
	@RequestMapping(value="/{id}/departamentos", method=RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> findDepartamento(@PathVariable("id") int id) {
		
		return new ResponseEntity<List<DepartamentoDTO>>(service.findDepartamentoByEmpresaId(id), HttpStatus.OK);
	}
	
}
