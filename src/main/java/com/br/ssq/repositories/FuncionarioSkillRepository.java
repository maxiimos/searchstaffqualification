package com.br.ssq.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.br.ssq.models.FuncionarioSkill;

@Repository
public interface FuncionarioSkillRepository extends JpaRepository<FuncionarioSkill, Integer> {

	@Transactional(readOnly=true)
	public List<FuncionarioSkill> findByFuncionarioId(@Param("id") Integer id);
	
}
