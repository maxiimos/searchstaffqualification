package com.br.ssq.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.ssq.models.Endereco;

public interface EnderecoRepository extends JpaRepository<Endereco, Integer> {

}
