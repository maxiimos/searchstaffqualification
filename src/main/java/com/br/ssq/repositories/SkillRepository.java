package com.br.ssq.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.br.ssq.models.Skill;
import com.br.ssq.models.relatorios.SkillNameAndCount;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Integer> {

	@Transactional(readOnly=true)
	public List<Skill> findByFuncionarioSkillsFuncionarioId(@Param("funcionarioId") Integer funcionarioId);

	@Query(value="SELECT COUNT(*) FROM SKILLS WHERE TRUNC(CRIADO)  BETWEEN TRUNC(SYSDATE-30) AND TRUNC(SYSDATE)",nativeQuery=true)
	public long contNewSkill();
	
	//SELECT PARA MYSQL E H2
//	@Query(value="SELECT TOP 6 DISTINCT NOME, COUNT(FS.SKILL_ID) AS QTD  "
//				+ "FROM SKILLS S "
//					+ "INNER JOIN FUNCIONARIO_SKILL FS ON (S.ID = FS.SKILL_ID)"
//				+"GROUP BY FS.SKILL_ID ORDER BY QTD DESC",nativeQuery=true)
	
	@Query(value="SELECT DISTINCT NOME, COUNT(FS.SKILL_ID) AS QTD  "
			+ "FROM SKILLS S "
				+ "INNER JOIN FUNCIONARIO_SKILL FS ON (S.ID = FS.SKILL_ID)"
			+"GROUP BY FS.SKILL_ID, NOME ORDER BY QTD DESC LIMIT 6",nativeQuery=true)
	public List<SkillNameAndCount> skillAndQtd();
	
	
}
