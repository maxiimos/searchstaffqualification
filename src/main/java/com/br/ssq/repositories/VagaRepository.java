package com.br.ssq.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.ssq.models.Vaga;

@Repository
public interface VagaRepository extends JpaRepository<Vaga, Integer>{

}
