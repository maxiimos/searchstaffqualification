package com.br.ssq.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.ssq.models.FormacaoAcademica;

public interface FormacaoAcademicaRepository extends JpaRepository<FormacaoAcademica, Integer> {

}
