package com.br.ssq.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.br.ssq.models.Funcionario;

@Repository
public interface FuncionarioRepository extends JpaRepository<Funcionario, Integer>, JpaSpecificationExecutor<Funcionario> {

	/* Procura funcionario por CPF */	
	@Transactional(readOnly=true)
	Funcionario findByCpf(@Param("cpf") String cpf);
	
	/* Procura funcionario por email */	
	@Transactional(readOnly=true)
	Funcionario findByEmail(@Param("email") String email);
	
	/* Procurar funcionarios pelo departamento */
	@Transactional(readOnly=true)
	List<Funcionario> findByDepartamentoNome(@Param("nome") String nome);
	
	/* Procurar funcionarios pelo id do departamento */
	@Transactional(readOnly=true)
	List<Funcionario> findByDepartamentoId(@Param("id") Integer id);
	
	/* Procurar funcionarios pelo nome do Skill */
	@Transactional(readOnly=true)
	List<Funcionario> findByFuncionarioSkillsSkillId(@Param("id") Integer id);
	
	/* Procurar funcionarios pelo nome do Skill */
	@Transactional(readOnly=true)
	List<Funcionario> findByFuncionarioSkillsSkillNome(@Param("nome") String nome);
	
	@Query(value="SELECT * FROM FUNCIONARIOS WHERE NOME = ?1 AND SOBRENOME = ?2", nativeQuery = true)
	List<Funcionario> findCustom(String nome, String sobrenome);
	
	@Query(value="SELECT COUNT(*) FROM FUNCIONARIOS WHERE TRUNC(DATA_CRIADO)  BETWEEN TRUNC(SYSDATE-30) AND TRUNC(SYSDATE)",nativeQuery=true)
	long contNewFuncionarios();
	
}
