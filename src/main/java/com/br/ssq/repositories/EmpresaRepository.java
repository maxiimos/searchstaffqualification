package com.br.ssq.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.br.ssq.models.Empresa;

@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, Integer>{

	@Transactional(readOnly=true)
	Empresa findByCnpj(@Param("cnpj") String cnpj);
	
}
