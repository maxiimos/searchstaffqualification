package com.br.ssq.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.ssq.models.SkillAprovacao;

@Repository
public interface SkillAprovacaoRepository extends JpaRepository<SkillAprovacao, Integer> {

}
