package com.br.ssq.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.br.ssq.models.Funcao;

@Repository
public interface FuncaoRepository extends JpaRepository<Funcao, Integer> {

	@Transactional(readOnly=true)
	@Query("SELECT obj FROM Funcao obj WHERE obj.departamento.id = :idDepartamento ORDER BY obj.nome ")
	public List<Funcao> findDepartamento(@Param("idDepartamento") Integer idDpeartamento);
	
}
